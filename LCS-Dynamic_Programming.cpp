/***
Longest Common Subsequence (LCS) - -Dynamic Programming

More detail at:
https://www.ics.uci.edu/~eppstein/161/960229.html
*/

#ifndef LCS_Dynamic_Programming_CPP
#define LCS_Dynamic_Programming_CPP

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>

using namespace std;

class LCS_DP{

      string    str1, str2, lcs_str;
      int       max_l;
      int       **L;
      vector    < pair<int,char> > lcs_vector;

  public:

      LCS_DP(string str1, string  str2){
          this-> str1 =  str1;
          this-> str2 =  str2;

          L = new int *[str1.length()+1];
          for(int i=0; i<= str1.length(); i++){
             L[i] = new int [ str2.length()+1];
             for(int j=0; j<= str2.length(); j++)
                 L[i][j] = 0;
          }
      }

      ~LCS_DP(){
         for(int i=0; i<=str1.length(); i++)
             delete [] L[i];
         delete [] L;
     }

     string get_lcs_str(){
         return lcs_str;
     }


     void find_lcs(){

        for(int i=0; i<= str1.length(); i++)
                 L[i][0] = 0;
        for(int i=0; i<= str2.length(); i++)
                 L[0][i] = 0;

        for(int i=1; i<=str1.length();i++)
             for(int j=1; j<=str2.length();j++){
                 if (str1.at(i-1)==str2.at(j-1)){
                     L[i][j] =  1 + L[i-1][j-1] ;
                 }
                 else{
                     L[i][j] =  max ( L[i-1][j] , L[i][j-1] );
                 }
             }
      }

      void print_L_matrix(){
          cout<<setw(2)<<""<<setw(4)<<setw(4)<<"";
          for (int i = 0; i<str2.length(); i++)
              cout<<setw(4)<<str2.at(i);
          cout<<endl<<endl;

          for (int i = 0; i<=str1.length(); i++){
               i>0 ? cout<<setw(2)<<str1.at(i-1): cout<<setw(2)<<"";
               for (int j = 0; j<= str2.length(); j++)
                     cout<<setw(4)<<L[i][j];

               cout<<endl;
          }
          cout<<endl;
      }

      string print_lcs (int m, int n){
          string str3 = "";

          for(int i=m; i>0;)
             for(int j=n; j>0;){
                if( i>0 && j >0 && str1.at(i-1) == str2.at(j-1)){
                      str3 =  str1.at(i-1) + str3 ;
                      i--; j--;
                 }
                 else if ( i>0 && j>0 && L[i][j-1] > L[i-1][j]){
                       j--;
                 }
                 else if ( i>0 && j>0){
                       i--;
                 }
                 else
                      break;
             }
             return str3 ;
      }

};

#endif // LCS_Dynamic_Programming_CPP

