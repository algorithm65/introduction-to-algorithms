/***
disjoint set
*/

#ifndef disjoint_set_CPP
#define disjoint_set_CPP

#include <iostream>
#include <iomanip>
#include <set>
#include <queue>
#include <vector>

using namespace std;

class disjoint_set{
    int * parent;
    int * set_rank;
    int set_size;

    public:
    disjoint_set(int set_size){
       this-> set_size = set_size;
       parent = new int [set_size];
       set_rank = new int [set_size];
       for (int i=0; i<set_size; i++ ){
           parent [i] = i;
           set_rank[i] = 0;
       }
    }
    int find_parent (int member){
         while (parent [member] != member)
             member = parent[member];
         return member;
    }
    void sets_union (int x, int y){

         int parent_x = find_parent(x);
         int parent_y = find_parent(y);


         if(parent_x == parent_y)
            return ;

         if (set_rank [parent_x]< set_rank [parent_y]){
               parent[parent_x] = parent_y;
               set_rank[parent_y]++;
         } else {
               parent[parent_y] = parent_x;
               set_rank[parent_x]++;
         }
    }

    void print_set_array(){
        for (int i=0;i<set_size; i++)
            cout<<parent[i]<<setw(3);
        cout<<setw(0)<<endl;
        for (int i=0;i<set_size; i++)
            cout<<i<<setw(3);
        cout<<setw(0)<<endl;
    }

    vector< vector<int> > get_sets(){
       vector< vector<int> > ret_vector;

       for (int i=0;i<set_size; i++){
           vector<int>  temp_vector;
           for (int j=0;j<set_size; j++)
             if(i == find_parent(j))
                 temp_vector.push_back (j);
           if(!temp_vector.empty())
              ret_vector.push_back (temp_vector);
       }
       return ret_vector;
    }

};

#endif // disjoint_set_CPP


