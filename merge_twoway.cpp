/***
Two way array merge
*/

#include <iostream>

using namespace std;

class twoway_merge{
      int *list1, *list2, *merged_list ;
      int  m,n;

  public:
      twoway_merge(int *list1, int *list2, int m, int n){

             this->list1 = list1;
             this->list2 = list2;
             this->m = m;
             this->n = n;
             this->merged_list = new int [m+n];
       }

       int* merge_list(){

             int i=0, j=0, k=0;
             for (; i<m &&j<n;  ){
                  if (list1[i]>=list2[j])
                       merged_list[k++] = list1[i++];
                  else
                       merged_list[k++] = list2[j++];

             }
             for ( ;i<m;  )
                  merged_list[k++] = list1[i++];
             for ( ;j<n; )
                  merged_list[k++] = list2[j++];

             return merged_list;
       }
};

