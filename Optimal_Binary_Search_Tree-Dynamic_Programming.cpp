/***
Optimal Binary Search Tree using the Dynamic Programming

1) This file contains the C++ class and functions necessary to implement
the Optimal Binary Search Tree using the Dynamic Programming

2) To run this program use the main file:
Optimal_Binary_Search_Tree_Dynamic_Programming_main.CPP
Which contains the function calls to demonstrate a sample run of the program

3) For more details about Optimal Binary Search using the Dynamic Programming, see:
https://www.radford.edu/~nokie/classes/360/dp-opt-bst.html

4) For more algorithm implementations, please visit:
https://gitlab.com/algorithm65/introduction-to-algorithms
*/

#ifndef Optimal_Binary_Search_Tree_Dynamic_Programming_CPP
#define Optimal_Binary_Search_Tree_Dynamic_Programming_CPP

#include <iostream>
#include <iomanip>

using namespace std;

class Optimal_BST_DP{

      int     no_of_nodes;
      float **cost_matrix;
      int   **root_matrix;
      int    *keys;
      int    *freq;

  public:
      Optimal_BST_DP(int n){
          this->no_of_nodes = n;

          keys = new int [n];
          freq = new int [n];

          cost_matrix = new float *[n];
          root_matrix = new int *[n];
          for(int i=0; i<n; i++){
             cost_matrix[i] = new float [n];
             root_matrix[i] = new int [n];
             for(int j=0; j<n; j++){
                 cost_matrix[i][j] = 0;
                 root_matrix[i][j] = -1;
             }
          }
      }

      ~Optimal_BST_DP(){
         delete []  keys;
         delete []  freq;

         for(int i=0; i<no_of_nodes; i++)
             delete [] cost_matrix[i];
         delete [] cost_matrix;
     }

      void input(){
         for(int i=0; i<no_of_nodes; i++)
               cin>>keys[i];
         for(int i=0; i<no_of_nodes; i++)
               cin>>freq[i];
      }

      int sum_freq(int i, int j)  {
          int s = 0;
          for (int m = i; m <= j; m++)
              s += freq[m];
          return s;
      }


      void find_min(){

          for(int i=0; i< no_of_nodes; i++)
              for(int j=0; j< no_of_nodes; j++)
                  if (i==j) cost_matrix[i] [j] = freq[i];
                  else if(i > j) cost_matrix[i] [j] = 0;
                  else if(i<j) cost_matrix[i][j] = INT_MAX;

          for(int diff=1; diff< no_of_nodes; diff++)
               for(int i=0; i< no_of_nodes -diff; i++){
                   int j = i+ diff;
                   if(i<j){
                       for(int k=i; k<=j ; k++){
                          float cost_1 = k-1<0 ? 0 : cost_matrix[i] [k-1];
                          float cost_2 = k+1>=no_of_nodes ? 0 :  cost_matrix[k+1] [j] ;
                          float temp = cost_1 + cost_2 +sum_freq(i,j);
                          if (temp < cost_matrix[i] [j]){
                                  cost_matrix[i] [j] = temp;
                                  root_matrix[i][j] = k;
                          }
                      }
                  }
              }
         }

      void print_cost_matrix(){
          for (int i = 0; i<  no_of_nodes; i++){
               for (int j = 0; j < no_of_nodes; j++)
                     cost_matrix[i][j] >= INT_MAX ?
                          cout<<setw(7)<<"INF":
                          cout<<setw(7)<<cost_matrix[i][j];
               cout<<endl; ;
          }
          cout<<endl;
      }

     void print_root_matrix(){
          for (int i = 0; i<  no_of_nodes; i++){
               for (int j = 0; j < no_of_nodes; j++)
                     root_matrix[i][j] == -1 ?
                          cout<<setw(7)<<"x":
                          cout<<setw(7)<<root_matrix[i][j];
               cout<<endl; ;
          }
          cout<<endl;
      }
};

#endif // Optimal_Binary_Search_Tree_Dynamic_Programming_CPP

