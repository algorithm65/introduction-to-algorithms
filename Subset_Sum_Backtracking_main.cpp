/***
Subset Sum Problem using Backtracking
Main file
*/

#include "Subset_Sum_Backtracking.cpp"

int main(){
    int n,s;
    cout<<"********************\n";
    cout<<"Subset Sum  Problem using Backtracking.\n";
    cout<<"Input: enter the target sum and number of elements\n";
    cout<<"followed by the elements\n";
    cout<<"********************\n";

    cin>>s;
    cin>>n;

    Subset_Sum_Backtrack  ss_bt (s,n) ;
    ss_bt.input();
    ss_bt.print_big_set();
    cout<<"\n";
    ss_bt.find_sum(0,0);
    cout<<"\n";
    cout<<"Total Solutions: "<< ss_bt.get_total_sol()<<endl;

 }
