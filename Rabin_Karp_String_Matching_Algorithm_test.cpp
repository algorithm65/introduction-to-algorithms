/***
Knuth-Morris-Pratt (KMP) String Matching Algorithm
Main file
*/

#include "Rabin_Karp_String_Matching_Algorithm.cpp"

int main(){
    string s1, s2;
    cout<<"********************\n";
    cout<<"Knuth-Morris-Pratt (KMP) String Matching Algorithm.\n";
    cout<<"Input: Two strings\n";
    cout<<"occurrence of the second string is searched inside the first\n";
    cout<<"********************\n";

    getline (cin,s1);
    getline (cin,s2);
    cout<<s1<<endl<<s2<<endl;
    Rabin_Karp  rk (s1, s2) ;
    rk.search_str();
    cout<<endl;

 }
