/***
N Queens Problem using Backtracking
Main file
*/

#include "N_Queens_Problem_Backtracking.cpp"

int main(){
    int n,c;
    cout<<"********************\n";
    cout<<"N Queens Problem using Backtracking.\n";
    cout<<"Input: enter the number of queens\n";
    cout<<"********************\n";

    cin>>n;
    cout<< "Queens: "<<n<<endl;

    N_Queen_Backtrack nq_bt (n) ;

    nq_bt.find_position(0);
    cout<<"Total Solutions: "<< nq_bt.get_total_sol()<<endl;

 }
