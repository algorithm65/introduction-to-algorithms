/***
 0/1 Knapsack using Branch and Bound

More details:
Knapsack Problems: Algorithms and Computer Implementations
by Paolo Toth and Silvano Martello
http://www.or.deis.unibo.it/kp/Chapter2.pdf
**/

#ifndef Knapsack_BB
#define Knapsack_BB

#include <iostream>
#include <iomanip>
#include <queue>
#include <set>
#include <algorithm>

using namespace std;

class item{
   public:
   float profit, weight;

    item(float p, float w){
       profit = p;
       weight = w;
    }
   item(){
      profit = -1;
      weight = -1;
   }
};


bool item_cmp(item a, item b)
{
    double r1 =  a.profit / a.weight;
    double r2 =  b.profit / b.weight;
    return r1 > r2;
}


class node{
  public:
     set<int> item_set;
     long     index;
     long     node_profit;
     long     node_upper;
     long     node_weight;

     node (){
        index  = -1;
        node_profit  = 0;
        node_weight  = 0;
        node_upper   = INT_MAX;
     }

};

class Knapsack_Branch{

      long    no_of_items;
      item    *items;
      float   capacity;
      float   global_upper, global_profit;
      long    call_count;

      queue < node > nodes_queue;
      set<int> max_set;

  public:
      Knapsack_Branch (int ca, int n){
          this->call_count = 0;
          this->no_of_items = n;
          this->capacity = ca;
          this->items = new item[n];
      }

      ~Knapsack_Branch(){
          delete [] items;

     }

     void input(){
         float p,w;
         for(int i=0; i< no_of_items; i++){
             cin>>items[i].profit>>items[i].weight;
         }
     }

     set<int> get_max_set(){
         return max_set;
     }
     long get_max_profit(){
         return global_profit;
     }

    void print_set(set<int> item_set){
         cout<<" { ";
         int c = 0;
         for (int i=0; i<no_of_items; ++i)
              if (item_set.count(i) != 0){
                  cout<<"t"<<i;
                  if (c++<item_set.size()-1) cout<< ", ";
              }
          cout<<" } ";
     }

     float find_profit_bound(int index){
         float c, w ;
         c = w = 0;
         int i;
         i = index<0 ? 0 : index;
         for ( ; i<no_of_items && w + items[i].weight<= capacity; i++){
                  c += items[i].profit;
                  w += items[i].weight;
             }
          if (i<no_of_items){
                c +=(capacity-w)*(items[i].profit/items[i].weight);
          }
          return c;
     }

     void start_search(){
          float temp_up, temp_c;
          node  temp_n, front_node, new_node;

          global_upper  = INT_MAX;
          global_profit   = 0;

          sort(items, items+no_of_items, item_cmp);
          cout<<"Items sorted by profitability:"<<endl;
          print_items();
          cout<<endl;

          temp_n.index = -1;
          temp_n.node_profit = 0;
          temp_n.node_weight = 0;
          nodes_queue.push(temp_n) ;

          while(!nodes_queue.empty()){
               front_node = nodes_queue.front();
               nodes_queue.pop();

               cout<<"Set: ";
               print_set(max_set) ;
               cout<<";   Profit: "<<global_profit<<endl;


               if(front_node.index == -1) new_node.index = 0;
               if(front_node.index >= no_of_items-1) continue;

               new_node.index = front_node.index + 1;
               new_node.item_set = front_node.item_set;
               new_node.item_set.insert(front_node.index + 1);

               new_node.node_weight = front_node.node_weight + items[new_node.index].weight;
               new_node.node_profit = front_node.node_profit + items[new_node.index].profit;

               if(new_node.node_weight <= capacity && new_node.node_profit > global_profit){
                        global_profit = new_node.node_profit;
                        max_set       =  new_node.item_set ;
               }

               if (new_node.node_weight >= capacity)
                     new_node.node_upper = 0;
               else
                     new_node.node_upper  =  find_profit_bound(new_node.index);

              if(new_node.node_upper > global_profit){
                     nodes_queue.push(new_node);
               }

               new_node.item_set.erase(front_node.index + 1);
               new_node.node_weight = front_node.node_weight ;
               new_node.node_profit = front_node.node_profit ;

               if (new_node.node_weight >= capacity)
                     new_node.node_upper = 0;
               else
                     new_node.node_upper  =  find_profit_bound(new_node.index-1);

               if( new_node.node_upper  >= global_profit){
                     nodes_queue.push(new_node);
               }
          }
     }

      void print_items(){
          cout<<setw(8)<<"Tasks:";
          for (int i = 0; i< no_of_items ; i++)
               cout<<setw(5)<<"t"<<i;
          cout<<endl;
          cout<<setw(8)<<"------------";
          for (int i = 0; i< no_of_items ; i++)
               cout<<"-----";
          cout<<endl;
          cout<<setw(8)<<"Profit:";
          for (int i = 0; i< no_of_items ; i++)
               cout<<setw(6)<<items[i].profit;
          cout<<endl;
          cout<<setw(8)<<"Weight:";
          for (int i = 0; i< no_of_items ; i++)
               cout<<setw(6)<<items[i].weight;
          cout<<endl;
      }
  };
#endif // Knapsack_BB

