/***
M-way Merge sort
*/
#include <iostream>
#include "merge_twoway.cpp"

using namespace std;

class merge_sort{

     int *list_a;
     int list_size;

     public:


     merge_sort(int *list_a, int list_size){
         this->list_size = list_size;
         this->list_a = list_a;
     }

     int* input_list(){
         int value;
         for (int i=0;i<list_size; i++){
              cin>>value;
              list_a[i] = value;
         }
         return list_a;
     }

     void print_a(){
        for (int i=0; i<list_size;i++)
            cout<<list_a[i]<<" ";
     }

     void sort_a(int * list_sub, int l, int h ){
         if (l>=h)
            return ;

         int m = ( l+h -1)/2;
         int n1 = m-l+1;
         int n2 = h-m;

         int *list1 = new int [n1];
         int *list2 = new int [n2];

         for (int i=0; i<n1; i++)
             list1 [i] = list_a[l+i];
         for (int j=0; j<n2; j++)
             list2 [j] = list_a[m+j+1];

         sort_a(list1, l, m);
         sort_a(list2, m+1, h);

         twoway_merge t_m(list1, list2, n1, n2);

         int * temp_a = t_m.merge_list();

         for (int k=l;k<=h;k++){
              list_a[k] = temp_a[k-l];
              list_sub[k-l] = temp_a[k-l];

         }
         delete list1;
         delete list2;
         return ;

     }

};

