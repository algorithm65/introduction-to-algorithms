/***
Recursive Matrix multiplication
***/

#include "matrix_recursive_multiplication.cpp"
 int main(){

   int size_m;
   cin>>size_m;
   matrix a(size_m, 0);
   a.input_m();
   a.print_m();
   cin>>size_m;
   matrix b(size_m, 0);
   b.input_m();
   b.print_m();

   mm_recursive c;
   matrix d = c.mm_re(a,b);
   d.print_m();
 }

