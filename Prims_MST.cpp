/***
Prim's algorithm to find MST
*/

#ifndef Prims_MST_CPP
#define Prims_MST_CPP

#include <iostream>
#include <iomanip>
#include <set>
#include <queue>

using namespace std;


class Prims_MST{

      int no_of_vertices;
      float *vertex_cost;
      int   *vertex_edge;
      float **adj_matrix;
      set<int> all_vertex_set;
      set<int> MST_vertex_set;
      priority_queue< pair<int, int> > vertex_pq;


  public:
      Prims_MST(int n){
          this->no_of_vertices = n;

          vertex_cost = new float[n];
          vertex_edge = new int[n];
          for(int i=0; i<n; i++){
               vertex_cost [i] = INT_MAX;
               vertex_edge [i] = -1;
          }
          adj_matrix = new float *[n];
          for(int i=0; i<n; i++)
            adj_matrix[i] = new float [n];

          for(int i=0; i<n; i++)
            for(int j=0; j<n; j++)
               adj_matrix[i][j] = 0;
      }

      ~Prims_MST(){
         for(int i=0; i<this->no_of_vertices; i++)
             delete [] adj_matrix[i];
          delete [] adj_matrix;

          delete [] vertex_cost;
          delete [] vertex_edge;
      }

      void input(){

           int v1,v2;
           float weight;

           while(1){
              cin>>v1;
              if (v1 == -1) break;
              cin>>v2>>weight;

              if (v1==v2) continue;
              else if (adj_matrix[v1][v2]==0){
                    adj_matrix[v1][v2] = weight;
                    adj_matrix[v2][v1] = weight;
              }
              else if (adj_matrix[v1][v2]>weight){
                    adj_matrix[v1][v2] = weight;
                    adj_matrix[v2][v1] = weight;
              }
          }
      }

      void find_mst(int start_index){

           vertex_cost[start_index] = 0;
           vertex_pq.push( make_pair( -vertex_cost[start_index], start_index) );

           for(int i=0; i<no_of_vertices; i++)
               all_vertex_set.insert(i);

           while(!all_vertex_set.empty()){
               pair<int, int> p1 = vertex_pq.top();
               int v = p1.second;
               vertex_pq.pop();
               all_vertex_set.erase(v);
               MST_vertex_set.insert(v);

               for(int w=0;w<no_of_vertices;w++)
                    if(adj_matrix[v][w] != 0 &&
                      all_vertex_set.count(w) &&
                        adj_matrix[v][w] < vertex_cost[w] ){
                            vertex_cost[w] = adj_matrix[v][w];
                            vertex_edge [w] = v;
                            vertex_pq.push( make_pair( -vertex_cost[w], w) );
                    }
           }

      }

      void print_MST_vertex(){
           set<int>::iterator it;
           for (it = MST_vertex_set.begin(); it != MST_vertex_set.end(); ++it)
                 cout << *it<<setw(5);
           cout<<endl;
      }

      void print_vertex_edge(){
          int cost = 0;
          cout<<"v1"<<setw(5)<<"v2"<<setw(8)<<"weight"<<setw(0)<<endl;
          for (int i = 0; i < no_of_vertices ; i++)
               if(vertex_edge[i]!=-1){
                     i < vertex_edge[i] ? cout <<i<<"  -  "<<vertex_edge[i] : cout <<vertex_edge[i]<<"  -  "<<i;
                     cout <<setw(5)<<adj_matrix[i][vertex_edge[i]]<<setw(0)<<endl ;
                     cost += adj_matrix[i][vertex_edge[i]];
               }
          cout<<endl;
          cout<<"Minimum cost: "<<cost<<endl;
      }

      void print_adj_matrix(){
          for (int i = 0; i<no_of_vertices; i++){
               for (int j = 0; j<no_of_vertices; j++){
                     cout<<adj_matrix[i][j];
                     j<no_of_vertices-1 ?  cout<<setw(4):  cout<<setw(0)<<endl; ;
               }
          }
         cout<<endl;
      }


};

#endif // Prims_MST_CPP

