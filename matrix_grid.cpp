/***
 Matrix Grid 2-D array class
 */
#ifndef matrix_grid_h
#define matrix_grid_h

#include <iostream>
#include "matrix_operations.cpp"

using namespace std;

class matrix_grid {

     matrix  **m_grid;
     int     size_g;
     int     total_grids;
     int     sub_size;

     public:
     matrix_grid();
     matrix_grid(int size_g);
     matrix_grid(int size_g, int sub_size, int init);
     int get_size_g();
     int get_sub_size();
     matrix get_m(int i, int j);
     matrix set_m(int i, int j, matrix a);
     void print_m(int i, int j);
     matrix add_m(int i, int j, matrix a);
     matrix_grid( matrix big_m, int  div_no);
     matrix join_m ();
     void print_grid();
     ~matrix_grid();
};

    matrix_grid:: matrix_grid(){
    }
    matrix_grid:: ~matrix_grid(){
       for (int i=0; i<size_g; i++)
            delete[] m_grid[i];
       delete[] m_grid;
    }

    matrix_grid:: matrix_grid(int size_g){
        this->size_g = size_g;
        m_grid = new matrix * [size_g];
        for(int i=0; i<size_g; i++ )
             m_grid[i] = new matrix [size_g];
     }

     matrix_grid:: matrix_grid(int size_g, int sub_size, int init){
        this->size_g = size_g;
        m_grid = new matrix * [size_g];
        for(int i=0; i<size_g; i++ ){
             m_grid[i] = new matrix [size_g];
             for(int j=0; j<size_g; j++ )
                m_grid[i][j] = matrix ( sub_size,init);
        }
     }

     int matrix_grid:: get_size_g(){
         return size_g;
     }
     int matrix_grid:: get_sub_size(){
         return sub_size;
     }
     matrix matrix_grid:: get_m(int i, int j){
         if (i<size_g && j<size_g)
             return (m_grid[i][j]);
     }
     matrix matrix_grid:: set_m(int i, int j, matrix a){
         if (i<size_g && j<size_g)
             return m_grid[i][j] = matrix(a);
     }
     void matrix_grid:: print_m(int i, int j){
         if (i<size_g && j<size_g)
              m_grid[i][j].print_m();
     }
     matrix matrix_grid:: add_m(int i, int j, matrix a){
         if (i<size_g && j<size_g)
             return m_grid[i][j] = m_grid[i][j] + matrix(a);
     }

     matrix_grid:: matrix_grid( matrix big_m, int  div_no){

         m_grid=NULL ;
         if (big_m.get_m_size() %2 != 0){
              cout<<"Matrix size is not a multiple of 2";
              return;
         }
         this->total_grids = (big_m.get_m_size()*big_m.get_m_size())/((big_m.get_m_size()/div_no)*(big_m.get_m_size()/div_no));
         this->sub_size = big_m.get_m_size()/div_no;
         this->size_g = div_no;

         m_grid = new matrix*[size_g] ;
         for (int i=0; i<size_g ; i++){
            m_grid[i] = new matrix[size_g] ;
            for (int j=0; j<size_g ; j++)
                m_grid[i][j] = matrix ( sub_size,-1);
         }

         for (int m1=0; m1< size_g ; m1++)
            for (int m2=0; m2< size_g ; m2++)
               for (int i=0; i<sub_size ; i++)
                   for(int j=0; j< sub_size ; j++){
                         m_grid[m1][m2].set_m_array(i,j,
                                  big_m.get_m_array(i+m1*(sub_size),j+m2*(sub_size)) );
                    }
     }


     matrix matrix_grid:: join_m (){
         int sub_size = m_grid[0][0].get_m_size();
         int big_m_size = size_g * sub_size;
         matrix big_m(big_m_size, -1);

         for (int m1=0; m1< size_g ; m1++)
            for (int m2=0; m2< size_g ; m2++)
               for (int i=0; i<sub_size ; i++)
                   for(int j=0; j< sub_size ; j++){
                        big_m.set_m_array( i+m1*(sub_size), j+m2*(sub_size),
                                                 m_grid[m1][m2].get_m_array(i, j) );
                    }
         return  big_m;
     }

     void matrix_grid:: print_grid(){
           for (int m1=0; m1< size_g ; m1++)
                  for (int m2=0; m2< size_g ; m2++){
                      m_grid[m1][m2].print_m();
                  }
     }

#endif
