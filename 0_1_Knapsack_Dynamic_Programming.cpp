/***
0/1 Knapsack - Dynamic Programming

https://www.educative.io/courses/grokking-dynamic-programming-patterns-for-coding-interviews/RM1BDv71V60
*/

#ifndef Knapsack_Dynamic_Programming_CPP
#define Knapsack_Dynamic_Programming_CPP

#include <iostream>
#include <iomanip>

using namespace std;

class Knapsack_DP{

      int     no_of_items;
      int     capacity;
      float **max_matrix;
      int    *values;
      int    *weights;

  public:
      Knapsack_DP(int n, int c){
          this->no_of_items = n;
          this->capacity = c;

          values = new int [n+1];
          weights= new int [n+1];

          max_matrix = new float *[n+1];
          for(int i=0; i<=n; i++){
             max_matrix[i] = new float [c+1];
             for(int j=0; j<=c; j++)
                 max_matrix[i][j] = 0;
          }
      }

      ~Knapsack_DP(){
         delete []  values;
         delete []  weights;

         for(int i=0; i<no_of_items; i++)
             delete [] max_matrix[i];
         delete [] max_matrix;
     }

      void input(){
         for(int i=1; i<=no_of_items; i++)
               cin>>values[i];
         for(int i=1; i<=no_of_items; i++)
               cin>>weights[i];
         values[0] = weights[0] = 0;
      }

      void find_max(){

           for(int i=0; i<=no_of_items; i++)
               max_matrix[i] [0] = 0;
           for(int j=0; j<=capacity; j++)
               max_matrix[0] [j] = 0;

           for(int i=1; i<=no_of_items; i++)
               for(int j=1; j<=capacity; j++)
                    if(weights[i] > j)
                         max_matrix[i] [j] = max_matrix[i-1] [j];
                    else
                         max_matrix[i] [j] = max_matrix[i-1] [j] >  max_matrix[i-1] [j-weights[i]] + values[i]?
                                             max_matrix[i-1] [j] :  max_matrix[i-1] [j-weights[i]] + values[i];

      }

      void print_max_matrix(){
          for (int i = 0; i<=no_of_items; i++){
               for (int j = 0; j<=capacity; j++)
                     cout<<setw(5)<<max_matrix[i][j];
               cout<<endl; ;
          }
          cout<<endl;
      }

      void print_selected_items(){
          int i = no_of_items;
          int j = capacity;
          int max_w = 0;
          string solution = "";
          while (i>0 && j>=0)
              if ( max_matrix[i] [j] == max_matrix[i-1] [j]){
                  solution =" 0 " + solution;
                  i--;
              }
              else {
                   solution =" 1 " + solution;
                   max_w += values[i];
                   j = j-weights[i]; i--;
              }
          cout<<"items:"<<solution<<endl;
          cout<<"Cost:"<<max_w<<endl;
      }
};

#endif // Knapsack_Dynamic_Programming

