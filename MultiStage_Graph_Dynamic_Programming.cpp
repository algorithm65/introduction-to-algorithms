/***
MultiStage Graph Dynamic Programming
*/

#ifndef MultiStage_Graph_Dynamic_Programming_CPP
#define MultiStage_Graph_Dynamic_Programming_CPP

#include <iostream>
#include <iomanip>
#include <set>
#include <queue>

using namespace std;

class MultiStage_DP{

      int   no_of_vertices, stages;
      float **adj_matrix;
      float *cost;
      int   *d;
      int   *path;

  public:
      MultiStage_DP(int n, int stages){
          this->no_of_vertices = n;
          this->stages = stages;

          cost = new float[n];
          d    = new int  [n];
          path = new int  [stages];
          adj_matrix = new float *[n];
          for(int i=0; i<n; i++){
             adj_matrix[i] = new float [n];
             for(int j=0; j<n; j++)
                 adj_matrix[i][j] = 0;
          }
      }

     ~MultiStage_DP(){
          for(int i=0; i<this->no_of_vertices; i++)
             delete [] adj_matrix[i];
          delete [] adj_matrix;
          delete [] cost;
          delete [] d ;
          delete [] path;
     }

     void input_edges(){

           int v1,v2;
           float weight;

           while(1){
              cin>>v1;
              if (v1 == -1) break;
              cin>>v2>>weight;

              if (v1==v2) continue;
              else if (adj_matrix[v1][v2]==0)
                    adj_matrix[v1][v2] = weight;
              else if (adj_matrix[v1][v2]>weight)
                    adj_matrix[v1][v2] = weight;

          }
     }

      void shortest_path(){

           cost[no_of_vertices-1] = 0;

           for(int i=no_of_vertices-2; i>=0; i--){
               float min_cost = INT_MAX;
               for(int k=i+1; k<no_of_vertices; k++)
                    if(adj_matrix[i][k] != 0 && adj_matrix[i][k] + cost[k] < min_cost){
                         min_cost = adj_matrix[i][k] + cost[k];
                         d[i] = k;
                    }
               cost[i]  = min_cost;
           }
           path[0] = 0;
           path[stages-1] = no_of_vertices-1;
           for(int i=1; i<=stages-1; i++)
               path[i] = d[path[i-1]];

      }

      void print_path(){
          for (int i=0;i<stages; i++)
            cout<<setw(5)<<path[i];
      }

      void print_arrays(){
            cout<<endl<<setw(5)<<"V:";
            for(int i=0; i<no_of_vertices; i++)
                cout<<setw(5)<<i;

            cout<<endl<<setw(5)<<"-----";
            for(int i=0; i<no_of_vertices; i++)
                cout<<setw(5)<<"-----";

            cout<<endl<<setw(5)<<"Cost:";
            for(int i=0; i<no_of_vertices; i++)
                cout<<setw(5)<<cost[i];

            cout<<endl<<setw(5)<<"d:";
            for(int i=0; i<no_of_vertices; i++)
                cout<<setw(5)<<d[i];
            cout<<endl;

            cout<<setw(5)<<"-----";
            for(int i=0; i<no_of_vertices; i++)
                cout<<setw(5)<<"-----";
            cout<<endl;

     }

      void print_adj_matrix(){
          for (int i = 0; i<no_of_vertices; i++){
               for (int j = 0; j<no_of_vertices; j++){
                     cout<<adj_matrix[i][j];
                     j<no_of_vertices-1 ?  cout<<setw(4):  cout<<setw(0)<<endl; ;
               }
          }
         cout<<endl;
      }
};

#endif // MultiStage_Graph_Dynamic_Programming_CPP

