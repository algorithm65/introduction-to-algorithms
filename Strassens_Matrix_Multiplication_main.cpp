/***
Strassen's Matrix Multiplication
***/

#include <iostream>
#include "Strassens_Matrix_Multiplication.cpp"

 int main(){

    int size_m;

    cin>>size_m;
    matrix a(size_m, 0);
    a.input_m();
    a.print_m();

    cin>>size_m;
    matrix b(size_m, 0);
    b.input_m();
    b.print_m();

    Strassen s;
    matrix d = s.Strassen_MM(a, b);
    d.print_m();
 }


