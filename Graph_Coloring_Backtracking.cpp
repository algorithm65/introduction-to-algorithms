/***
Author: Maruf Ahmed
@https://gitlab.com/algorithm65/introduction-to-algorithms

Graph Coloring Problem using Backtracking

https://www.cs.odu.edu/~zeil/cs361/web/website/Lectures/npprobs/pages/ar01s01s01.html
*/

#ifndef Graph_Color_Backtrack_CPP
#define Graph_Color_Backtrack_CPP

#include <iostream>
#include <iomanip>

using namespace std;

class Graph_Color_Backtrack{

      long   no_of_vertex, no_of_color;
      float  **adj_matrix;
      int    *v_color;

      long   total_sol;
      long   call_count;

  public:
      Graph_Color_Backtrack(int v_no, int c_n0){
          this->call_count = 0;
          this->total_sol  = 0;

          this->no_of_vertex = v_no;
          this->no_of_color  = c_n0;

          adj_matrix = new float *[no_of_vertex];
          v_color    = new int    [no_of_vertex];


          for(int i=0; i<no_of_vertex; i++){
             v_color[i] = 0;

             adj_matrix[i] = new float [no_of_vertex];
             for(int j=0; j<no_of_vertex; j++)
                 adj_matrix[i][j] = 0.0;
          }
     }

      ~Graph_Color_Backtrack(){
          for(int i=0; i<no_of_vertex; i++)
             delete [] adj_matrix[i];
          delete [] adj_matrix;
          delete [] v_color;

     }

     int get_total_sol(){
         return total_sol;
     }

      void input(){

           int v1,v2;
           float weight;

           while(1){
              cin>>v1;
              if (v1 == -1) break;
              cin>>v2;
              //cin>>weight;
              weight = 1;
              if (v1==v2) continue;
              else if (adj_matrix[v1][v2]==0){
                    adj_matrix[v1][v2] = weight;
                    adj_matrix[v2][v1] = weight;
              }
              else if (adj_matrix[v1][v2]>weight){
                    adj_matrix[v1][v2] = weight;
                    adj_matrix[v2][v1] = weight;
              }
          }
      }

     bool check_color(){
          for (int i = 0; i<no_of_vertex; i++){
               for (int j = 0; j<no_of_vertex; j++){
                     if (adj_matrix[i][j] &&
                         v_color[i] !=0   &&
                         v_color[j] == v_color[i])
                             return  false;
               }
          }

         return true;
      }


     void find_color( int index){

          //cout<<"call_count: "<<call_count++<<endl;
          if (index>no_of_vertex)
              return;

          else if (index==no_of_vertex &&
                                  check_color()) {
              total_sol++;
              cout<<"solution no."<<total_sol<<endl;
              print_c_set();
              return ;
          }
          else
             for(int c=1; c<=no_of_color; c++){
                 v_color[index] = c;
                 if (check_color())
                     find_color( index+1 );
                 v_color[index] = 0;
            }
      }


      void print_c_set(){
          cout<<"{";
          for (int i = 0; i< no_of_vertex ; i++){
               cout<<setw(3)<<v_color[i];
               if(i<no_of_vertex-1)cout<<",";
          }
          cout<<"}"<<endl;
      }

      void print_adj_matrix(){
          for (int i = 0; i<no_of_vertex; i++){
               for (int j = 0; j<no_of_vertex; j++){
                     cout<<adj_matrix[i][j];
                     j<no_of_vertex-1 ?  cout<<setw(4):  cout<<setw(0)<<endl; ;
               }
          }
         cout<<endl;
      }

  };

#endif // Graph_Color_Backtrack_CPP
