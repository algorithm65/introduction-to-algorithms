/***
Job Sequencing with Deadline - Branch and Bound
Main file
*/

#include "Job_Sequencing_with_Deadline_Branch_and_Bound.cpp"

int main(){
    int n;
    cout<<"********************\n";
    cout<<"Job Sequencing with Deadline Branch and Bound.\n";
    cout<<"Input: enter the number of jobs\n";
    cout<<"followed by the penalty, deadline, and time for each\n";
    cout<<"********************\n";

    cin>>n;

    Job_Sequencing_BB  js_bb (n) ;
    js_bb.input();
    js_bb.print_jobs();
    cout<<"\n";
    js_bb.start_search();
    cout<<"\n";
    cout<<"Final set:";
    js_bb.print_set( js_bb.get_min_set() );

 }
