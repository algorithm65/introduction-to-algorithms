/***
Knuth-Morris-Pratt (KMP) String Matching Algorithm

More detail at:
http://courses.csail.mit.edu/6.006/spring11/rec/rec06.pdf
*/

#ifndef Rabin_Karp_String_Matching
#define Rabin_Karp_String_Matching

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>

using namespace std;

class Rabin_Karp{
      string str1;
      string pattern;
      long   s_len;
      long   p_len;
      int    digits, h, q;
      long   s_hash, p_hash;

 public:
    Rabin_Karp(string str1, string pattern){
       this->str1 = str1;
       this->pattern = pattern;
       s_len = str1.size();
       p_len = pattern.size();

       s_hash = p_hash = 0;
       digits = 256;
       h = 1;
       q = 101;
    }

    void search_str(){

         for (int i= 0; i< p_len-1 ; i++)
                h= (h * digits) % q;

         for (int i= 0 ; i < p_len;  i++){
              s_hash = ( digits*s_hash  + pattern.at(i))%q;
              p_hash = ( digits*p_hash  + str1.at(i))%q;
         }

         for (int i=0; i<= s_len - p_len; i++){

               if (s_hash == p_hash){
                   int j= 0;
                   while (j< p_len && str1.at(i+j) == pattern.at(j))
                          j++;
                   if (j == p_len)
                       cout<<"Pattern found at "<<i<<endl;
               }
               if (i < s_len - p_len){
                    s_hash = ( (s_hash - str1.at(i)*h )*digits + str1.at(i+p_len) ) % q;

                    if (s_hash < 0) s_hash += q;
                }
         }

    }
};







#endif // Rabin_Karp_String_Matching
