/***
Optimal Binary Search Tree using the Dynamic Programming

1) This file contains the main function run the program in the file:
Optimal_Binary_Search_Tree_Dynamic_Programming.CPP

2) For more details about Optimal Binary Search using the Dynamic Programming, see:
https://www.radford.edu/~nokie/classes/360/dp-opt-bst.html

3) For more algorithm implementations, please visit:
https://gitlab.com/algorithm65/introduction-to-algorithms
*/

/*****************************************************
Sample input:
4
10 12 16 21
4 2 6 3

Sample output:
Total nodes: 4
Cost Matrix:
      4      8     20     26
      0      2     10     16
      0      0      6     12
      0      0      0      3

Root Matrix:
      x      0      2      2
      x      x      2      2
      x      x      x      2
      x      x      x      x
/*****************************************************/

#include "Optimal_Binary_Search_Tree-Dynamic_Programming.cpp"

int main(){
    int n,c;
    cout<<"********************\n";
    cout<<"Optimal Binary Search Tree using the Dynamic Programming.\n";
    cout<<"Input: First input the number of nodes\n";
    cout<<"Then all the keys, then followed by their frequencies \n";
    cout<<"********************\n";

    cin>>n;
    cout<< "Total nodes: "<<n<<endl;

    Optimal_BST_DP obst_dp (n) ;
    obst_dp.input();
    obst_dp.find_min();
    cout<<"Cost Matrix:"<<endl;
    obst_dp.print_cost_matrix();
    cout<<"Root Matrix:"<<endl;
    obst_dp.print_root_matrix();

}
