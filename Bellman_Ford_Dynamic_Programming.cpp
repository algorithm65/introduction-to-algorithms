/***
Bellman Ford Algorithm - Single Source Shortest Path - Dynamic Programming
*/

#ifndef Bellman_Ford_Dynamic_Programming_CPP
#define Bellman_Ford_Dynamic_Programming_CPP

#include <iostream>
#include <iomanip>


using namespace std;

class Bellman_Ford{

      int no_of_vertices;
      float **adj_matrix;
      float  *dist;
      int    *pred;
      int    source;

  public:
      Bellman_Ford(int n){
          this->no_of_vertices = n;
          dist = new float [n];
          pred = new int  [n];
          adj_matrix = new float *[n];
          for(int i=0; i<n; i++){
             adj_matrix[i] = new float [n];
             for(int j=0; j<n; j++)
                 adj_matrix[i][j] = 0;
          }
      }

      ~Bellman_Ford(){
         delete []  dist;
         delete []  pred;
         for(int i=0; i<this->no_of_vertices; i++)
             delete [] adj_matrix[i];
          delete [] adj_matrix;
     }

      void input(){

           int v1,v2;
           float weight;

           while(1){
              cin>>v1;
              if (v1 == -1) break;
              cin>>v2>>weight;

              if (v1==v2) continue;
              else if (adj_matrix[v1][v2]==0){
                    adj_matrix[v1][v2] = weight;
                    //adj_matrix[v2][v1] = weight;
              }
              else if (adj_matrix[v1][v2]>weight){
                    adj_matrix[v1][v2] = weight;
                    //adj_matrix[v2][v1] = weight;
              }
          }
      }

      int Single_Source_Shortest_Path(int source){
           this->source = source;
           for(int i=0; i<no_of_vertices; i++){
               dist[i] = INT_MAX;
               pred[i] = -1;
           }
           dist[ source ] = 0;

           for(int i=0; i<no_of_vertices-1; i++)
               for(int u=0; u<no_of_vertices; u++)
                   for(int v=0; v<no_of_vertices; v++)
                       if(adj_matrix[u][v] != 0 &&
                          dist[u] + adj_matrix[u][v] < dist[v]){
                               dist[v] = dist[u] + adj_matrix[u][v];
                               pred[v] = u;
                       }

           for(int u=0; u<no_of_vertices; u++)
               for(int v=0; v<no_of_vertices; v++)
                   if(adj_matrix[u][v] != 0 &&
                          dist[u] + adj_matrix[u][v] < dist[v]){
                              cout<<"Error: Negative cycle in the graph"<<endl;
                              return -1;
                          }
          return 0;
      }

      void print_adj_matrix(){
          for (int i = 0; i<no_of_vertices; i++){
               for (int j = 0; j<no_of_vertices; j++)
                     cout<<setw(4)<<adj_matrix[i][j];
               cout<<endl; ;
          }
          cout<<endl;
      }

      void print_arrays(){
           cout<<setw(7)<<" ";
           for(int i=0; i<no_of_vertices; i++)
               cout<<setw(4)<<"v"<<i;
           cout<<endl;
           cout<<setw(7)<<"dist:";
           for(int i=0; i<no_of_vertices; i++)
               cout<<setw(5)<<dist[i];
           cout<<endl;
           cout<<setw(7)<<"pred:";
           for(int i=0; i<no_of_vertices; i++)
               cout<<setw(5)<<pred[i];
           cout<<endl;
      }


};

#endif // Bellman_Ford_Dynamic_Programming

