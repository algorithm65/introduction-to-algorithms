/***
Floyd Warshall Dynamic Programming
Main file
*/

#include "Floyd_Warshall_Dynamic_Programming.cpp"

int main(){
    int n;
    cout<<"********************\n";
    cout<<"Floyd Warshall algorithm.\n";
    cout<<"Input: First input the number of nodes\n";
    cout<<"Followed by each edge in the form: v1 v2 weight \n";
    cout<<"Edge input stops with -1\n";
    cout<<"********************\n";

    cin>>n;
    cout<< "Total Nodes: "<<n<<endl;
    Floyd_Warshall_DP fw_dp (n) ;
    fw_dp.input();
    fw_dp.print_adj_matrix();
    fw_dp.Path_Reconstruct();

    cout<<"Dist matrix: "<<endl;
    fw_dp.print_dist_matrix();
    cout<<"Next matrix: "<<endl;
    fw_dp.print_next_matrix();



}
