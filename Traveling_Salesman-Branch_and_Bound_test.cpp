/***
Traveling Salesman Problem - Branch and Bound
Main file
*/

#include "Traveling_Salesman-Branch_and_Bound.cpp"

int main(){
    int n;
    cout<<"********************\n";
    cout<<"Traveling Salesman Problem - Branch and Bound.\n";
    cout<<"Input: enter the number of nodes\n";
    cout<<"Followed by each edge in the form: <v1 v2 weight> \n";
    cout<<"Edge input stops with -1\n";
    cout<<"********************\n";

    cin>>n;

    Traveling_Salesman_BB  tsp_bb (n) ;
    tsp_bb.input();
    cout<<"\nAdj matrix:\n";
    tsp_bb.print_adj_matrix(n, tsp_bb.get_adj_matrix());
    cout<<"\n";
    tsp_bb.start_search();

    cout<<"Min path:";
    tsp_bb.print_set( tsp_bb.get_min_set() );
    cout<<"\nMin cost: "<<tsp_bb.get_min_cost()<<endl;;

 }
