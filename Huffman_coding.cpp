/***
Huffman coding algorithm
using inverse tree

****/

#ifndef Huffman_coding
#define Huffman_coding

#include <iostream>
#include <iomanip>
#include <queue>
#include <vector>

using namespace std;

class huffman_node{
      string value;
      float frequency;
      public:
      int serial, left_serial, right_serial;

      public:
      huffman_node(){
          value= "";
          frequency = -1;
          serial = left_serial = right_serial = -1;
      }

      ~huffman_node(){
      }

      huffman_node (string val, float freq){
           this->value =val;
           this->frequency = freq;
           serial = left_serial = right_serial = -1;
      }

      float get_frequency () const{
          return frequency;
      }
      string get_value () {
          return value;
      }
      int get_serial () {
          return serial;
      }
      int get_left () {
         return left_serial;
      }
      int get_right () {
          return right_serial;
      }

};

 bool operator<(const huffman_node& n1, const huffman_node& n2) {
      return n1.get_frequency() > n2.get_frequency();
}


class huffman_tree{

     priority_queue <huffman_node> p_q;
     vector<huffman_node> tree_vector;
     int element_no, global_tree_serial;

     public:
     huffman_tree(){
        element_no = global_tree_serial=0;
     }
     int get_global_serial (){
        return global_tree_serial;
     }

     void input(int no){
          float freq;
          string val;
          cout<<"Char"<<setw(10)<<"Frequency"<<endl;
          for (int i=0; i<no; i++){
              cin>>val>>freq;
              cout<<val<<setw(10)<<freq<<endl;
              huffman_node  new_node(val, freq);
              p_q.push(new_node);
              element_no++;
          }
     }

    void build_tree(){
        int current_highest = -1;
        huffman_node left, right;
        while (!p_q.empty()) {

            left= p_q.top() ;
            p_q.pop();

            right= p_q.top() ;
            p_q.pop();
            //cout<<""<<left.get_frequency()+ right.get_frequency()<<endl;
            huffman_node new_node("*", left.get_frequency()+ right.get_frequency());

            if (left.serial == -1) left.serial  = global_tree_serial ++;
            if (right.serial == -1) right.serial = global_tree_serial ++;

            new_node.left_serial = left.serial;
            new_node.right_serial = right.serial;
            new_node.serial = global_tree_serial ++;

            if (left.serial > current_highest){
                tree_vector.push_back(left);
                current_highest = left.serial;
            }
            if (right.serial > current_highest){
                tree_vector.push_back(right);
                current_highest = right.serial;
            }
            if (new_node.serial > current_highest){
                tree_vector.push_back(new_node);
                current_highest = new_node.serial;
            }
            p_q.push(new_node);
            if (p_q.size()<=1)
                  break;
       }
   }

   void print_tree(){
       int vec_size = tree_vector.size();
       for (int i=0; i<vec_size; i++) {
            huffman_node temp = tree_vector[i] ;
            cout<<temp.get_value()<<": "<<temp.get_frequency()<<" ("<<temp.serial<<") "
                                                         <<temp.left_serial<<" "<<temp.right_serial<<endl;
       }
   }

   void traverse_tree(int index, string coding){
      if (  tree_vector[index].get_left() == -1  ){
               cout<<tree_vector[index].get_value()<<": "<<coding<<endl;
               return ;
       }
       else{
            traverse_tree(tree_vector[index].get_left(), coding+"0");
            traverse_tree(tree_vector[index].get_right(), coding+"1");
       }
   }

};
#endif // Huffman_coding




