/***
greedy_job_sequence
*/

#ifndef greedy_job_sequence_h
#define greedy_job_sequence_h

#include <string>
#include <iostream>
#include <iomanip>
using namespace std;

class job{
   string name;
   float profit;
   int deadline;

   public:
   job(){
   }
   job (string name, float profit, int deadline){
          this->name = name;
          this->profit = profit;
          this->deadline = deadline;
   }

   job& operator= (const job & b){
          this->name = b.name;
          this->profit = b.profit;
          this->deadline = b.deadline;
          return *this;
   }

   string get_name(){
       return name;
   }
   float get_profit(){
       return profit;
   }
   int get_deadline(){
    return deadline;
   }
};

class job_sequence{

    job *job_list;
    int  size_l;
    int  job_slot_no;
    string *job_slots;

    public:
    job_sequence (int size_l){
        this->job_slot_no = -1;
        this->size_l = size_l;
        job_list = new job[size_l];

    }

    ~job_sequence (){
        delete [] job_list ;
        delete [] job_slots;
    }


    void input_list(){
        string name;
        float profit;
        int deadline;

        for(int i=0; i<size_l; i++){
             cin>>name>>profit>>deadline;
             job_list[i] = job(name, profit, deadline);
             if(deadline > job_slot_no)
                job_slot_no = deadline;
        }

        job_slots = new string[job_slot_no];
        for(int i=0; i<job_slot_no; i++){
            job_slots[i] = "";
        }

    }

    void print_list(){
       cout<<"Name"<<setw(10)<<"Profit"<<setw(10)<<"Deadline"<<endl;

       for(int i=0; i<size_l; i++)
             cout<<job_list[i].get_name()<<setw(10)<<job_list[i].get_profit()<<setw(10)<<job_list[i].get_deadline()<<endl;
       cout<<endl;
    }

    void sort_list(int l, int h){
         int j ;
         if (l<h){
            j = partition_list (l,h);
            sort_list (l,j);
            sort_list (j+1, h);
         }
     }

     int partition_list (int l, int h){
          job temp;
          int i=l, j=h;
          float pivot = job_list[l].get_profit();

          while(i<j){
             while(job_list[i].get_profit()>=pivot)
                 i++;
             while (job_list[j].get_profit()<pivot)
                 j--;
             if(i<j){
                 temp = job_list[i];
                 job_list[i] = job_list[j];
                 job_list[j] = temp;
              }
          }
          temp = job_list[l];
          job_list[l] = job_list[j];
          job_list[j] = temp;
          return j;
     }
   /***/
     string* sequence(){
          sort_list(0,size_l-1);

          for (int i=0; i<size_l; i++){
              int job_dealine = job_list[i].get_deadline() - 1;

              for(int j = job_dealine; j>=0; j--)
                    if(job_slots[j]==""){
                         job_slots[j] = job_list[i].get_name();
                         break;
                    }
          }
          return job_slots;
     }
     void print_sequence(){
          cout<<"{";
          for (int i=0;i<job_slot_no; i++){
            cout<<job_slots[i];
            i<job_slot_no-1 ? cout<<",  ": cout<<"";
          }
          cout<<"}";
          cout<<endl;
     }
   /**/


};
#endif
