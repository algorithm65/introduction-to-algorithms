/***
 0/1 Knapsack using Branch and Bound
Main file
*/

#include "0_1_Knapsack-Branch_and_Bound.cpp"

int main(){
    int n,c;
    cout<<"********************\n";
    cout<<" 0/1 Knapsack using Branch and Bound.\n";
    cout<<"Input: enter the capacity and number of jobs\n";
    cout<<"followed by the profit, and weight for each\n";
    cout<<"********************\n";
    cin>>c;
    cin>>n;

    Knapsack_Branch  kn_bb (c,n) ;
    kn_bb.input();
    //kn_bb.print_items();
    cout<<"\n";
    kn_bb.start_search();
    cout<<"\n";
    cout<<"Max set:";
    kn_bb.print_set( kn_bb.get_max_set() );
    cout<<";   Max profit: "<<kn_bb.get_max_profit()<<endl;;

 }
