/***
disjoint set main function
*/

#include  "disjoint_set.CPP"

int main(){

   disjoint_set d_s(10);
   //d_s.print_set_array();
   d_s.sets_union(1,5);
   d_s.sets_union(5,9);
   d_s.sets_union(2,7);
   d_s.sets_union(7,4);
   d_s.sets_union(1,2);
   d_s.sets_union(3,0);
   d_s.sets_union(8,6);
   //d_s.print_set_array();
   cout<<"disjoint sets: "<<endl;
   vector< vector<int> > ret_vector = d_s.get_sets();
   for (int i=0; i<ret_vector.size(); i++){
        cout<<"{";
        for (int j=0; j<ret_vector[i].size(); j++){
             cout << ' ' << ret_vector[i][j];
             j<ret_vector[i].size()-1 ? cout<<",": cout<<" }";
        }
        cout<<endl;
    }
}
