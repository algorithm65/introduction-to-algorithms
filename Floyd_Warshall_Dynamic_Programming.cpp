/***
Floyd Warshall Dynamic Programming
*/

#ifndef Floyd_Warshall_Dynamic_Programming_CPP
#define Floyd_Warshall_Dynamic_Programming_CPP

#include <iostream>
#include <iomanip>
#include <vector>

using namespace std;

class Floyd_Warshall_DP{

      int    no_of_vertices;
      float  **adj_matrix;
      float  **dist;
      int    **next;

  public:

      Floyd_Warshall_DP(int n){
          this->no_of_vertices = n;

          adj_matrix = new float *[n];
          dist       = new float *[n];
          next       = new int   *[n];

          for(int i=0; i<n; i++){
             adj_matrix[i] = new float [n];
             dist[i]       = new float [n];
             next[i]       = new int   [n];
             for(int j=0; j<n; j++){
                 adj_matrix[i][j] = 0;
                 dist[i] [j] = INT_MAX;
                 next[i] [j] = -1;
             }
          }
      }

      ~Floyd_Warshall_DP(){
         for(int i=0; i<this->no_of_vertices; i++){
             delete [] adj_matrix[i];
             delete [] dist[i];
             delete [] next[i] ;
         }
          delete [] adj_matrix;
          delete [] dist;
          delete [] next;
     }

      void input(){

           int v1,v2;
           float weight;

           while(1){
              cin>>v1;
              if (v1 == -1) break;
              cin>>v2>>weight;

              if (v1==v2) continue;
              else if (adj_matrix[v1][v2]==0)
                    adj_matrix[v1][v2] = weight;
              else if (adj_matrix[v1][v2]>weight)
                    adj_matrix[v1][v2] = weight;
           }
      }

      void Path_Reconstruct(){

          for(int u=0; u<no_of_vertices; u++)
               for(int v=0; v<no_of_vertices; v++)
                    if (v==u){
                           dist[u] [v] =  0;
                           next[u] [v] =  v;
                    }
                    else if(adj_matrix[u][v] != 0) {
                           dist[u] [v] = adj_matrix[u] [v];
                           next[u] [v] =  v;
                    }

          for(int w=0; w<no_of_vertices; w++)
               for(int u=0; u<no_of_vertices; u++)
                     for(int v=0; v<no_of_vertices; v++)
                     if (dist [u] [v] > dist [u] [w] + dist [w][v]){
                         dist [u] [v] = dist [u] [w] + dist [w][v];
                         next [u] [v] = next [u] [w];
                     }

      }
      vector <int> get_path (int u, int v){
           vector <int> path_vector;
           if (next[u] [v] == -1)
                return path_vector;
           path_vector.push_back(u);
           while (u != v) {
              u = next [u] [v];
              path_vector.push_back(u);
           }
           return path_vector;
      }


      void print_adj_matrix(){
          for (int i = 0; i<no_of_vertices; i++){
               for (int j = 0; j<no_of_vertices; j++){
                     cout<<adj_matrix[i][j];
                     j<no_of_vertices-1 ?  cout<<setw(4):  cout<<setw(0)<<endl; ;
               }
          }
         cout<<endl;
      }

       void print_dist_matrix(){
          for (int i = 0; i<no_of_vertices; i++){
               for (int j = 0; j<no_of_vertices; j++){
                     dist[i][j]>32000 ?  cout<<"INF" :cout<<dist[i][j];
                     j<no_of_vertices-1 ?  cout<<setw(5):  cout<<setw(0)<<endl; ;
               }
          }
         cout<<endl;
      }
      void print_next_matrix(){
          for (int i = 0; i<no_of_vertices; i++){
               for (int j = 0; j<no_of_vertices; j++){
                     cout<<next[i][j];
                     j<no_of_vertices-1 ?  cout<<setw(4):  cout<<setw(0)<<endl; ;
               }
          }
         cout<<endl;
      }

};

#endif // Floyd_Warshall_Dynamic_Programming

