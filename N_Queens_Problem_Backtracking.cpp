/***
N Queens Problem using Backtracking

http://users.monash.edu/~lloyd/tildeAlgDS/Recn/Queens/
*/

#ifndef N_Queens_Problem_Backtracking_CPP
#define N_Queens_Problem_Backtracking_CPP

#include <iostream>
#include <iomanip>

using namespace std;

class N_Queen_Backtrack{

      int     size_of_board;
      int   **board;
      int    *position;
      int     call_count;
      int     total_sol;

  public:
      N_Queen_Backtrack(int n){
          call_count = 0;
          total_sol  = 0;
          this->size_of_board = n;
          position = new int [n];

          board = new int *[n];
          for(int i=0; i<n; i++){
             board[i] = new int [n];
             for(int j=0; j<n; j++)
                 board[i][j] = 0;
          }
      }

      ~N_Queen_Backtrack(){
         for(int i=0; i<size_of_board; i++)
             delete [] board[i];
         delete [] board;
     }

     int get_total_sol(){
         return total_sol;
     }

     bool check(int r, int c){

          int q_count =0;
          for (int j=0; j<size_of_board; j++)
             if (board[j][c]==1)
                  if ( ++q_count > 1) return false  ;

           int i,j;
           q_count =0;
           for (i=r, j=c; i>=0 && j>=0; i--, j--)
               if (board[i][j]==1)
                   if ( ++q_count > 1) return false  ;

           q_count =0;
           for (i=r, j=c; i>=0 && j<size_of_board; i--, j++)
               if (board[i][j]==1)
                    if ( ++q_count > 1) return false  ;
            return true;
     }



     void find_position(int row){
          //cout<<"call_count: "<<call_count++<<endl;
          if (row >= size_of_board ) {
             total_sol++;
             cout<<"No. "<<total_sol<<endl;
             print_board();
             return ;
          }

          for(int i=0; i< size_of_board; i++){
               board[row][i] = 1;
               if(check(row,i)==true)
                   find_position(row+1) ;
               board[row][i] = 0;
          }
      }

      void print_board(){
          for (int i = 0; i< size_of_board; i++){
               for (int j = 0; j< size_of_board; j++)
                     board[i][j]==1? cout<<setw(2)<<"q": cout<<setw(2)<<"." ;
               cout<<endl; ;
          }
          cout<<endl;
      }
  };

#endif // N_Queens_Problem_Backtracking_CPP


