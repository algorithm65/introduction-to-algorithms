/***
Quick sort
*/

#include <iostream>
using namespace std;

class quick_sort{

     int * list_a;
     int   list_size;

     public:

     quick_sort (int list_size){
         this->list_size = list_size;
         this->list_a = new int [list_size];
     }
     ~quick_sort (){
         delete  [] list_a ;
     }

     int* input_list(){
         for (int i=0;i<list_size; i++)
              cin>>list_a[i];
         return list_a;
     }
     void print_a(){
        for (int i=0; i<list_size;i++)
            cout<<list_a[i]<<" ";
     }

     int partition_a (int l, int h){

         int pivot = list_a[l];
         int i=l, j=h, temp;

         while (i<j){
            for ( ;list_a[i] <= pivot; i++) ;
            for ( ;list_a[j] > pivot; j--) ;
            if(i<j){
                temp = list_a[i];
                list_a[i] = list_a[j];
                list_a[j] = temp;
            }
         }
         temp = list_a[l];
         list_a[l] = list_a[j];
         list_a[j] = temp;

         return j;
     }

     int q_sort (int l, int h){
         int j;

         if (l<h) {
            j = partition_a(l,h);
            q_sort (l, j);
            q_sort (j+1, h);
         }
     }
};

int main(){
    int n;
    cin>>n;
    quick_sort q_s(n);
    q_s.input_list();
    q_s.print_a();
    q_s.q_sort (0, n-1);
    cout<<endl;
    q_s.print_a();
}
