/***
Bellman Ford Algorithm - Single Source Shortest Path - Dynamic Programming
Main file
*/

#include "Bellman_Ford_Dynamic_Programming.cpp"

int main(){
    int n;
    cout<<"********************\n";
    cout<<"Bellman Ford Algorithm to find Single Source Shortest Path using Dynamic Programming.\n";
    cout<<"Input: First input the number of nodes\n";
    cout<<"Followed by each edge in the form: v1 v2 weight \n";
    cout<<"Edge input stops with -1\n";
    cout<<"********************\n";

    cin>>n;
    cout<< "Total Nodes: "<<n<<endl;
    Bellman_Ford  bf_dp (n) ;
    bf_dp.input();
    bf_dp.print_adj_matrix();
    int flag = bf_dp.Single_Source_Shortest_Path(0);
    if (flag ==0)
        bf_dp.print_arrays();


}
