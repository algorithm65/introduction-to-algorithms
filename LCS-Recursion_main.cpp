/***
Longest Common Subsequence (LCS) - Recursion
Main file
*/

#include "LCS-Recursion.cpp"

int main(){
    string str1,str2;
    cout<<"********************\n";
    cout<<"Longest Common Subsequence (LCS) - Recursion.\n";
    cout<<"Input: Input two strings\n";
    cout<<"********************\n";

    cin>>str1;
    cin>>str2;

    LCS_Recursion lcs_r (str1,str2) ;
    int l = lcs_r.find_lcs(0, 0);
    cout<<endl;
    lcs_r.print_L_matrix();
    cout<<"LCS: "<<l<<endl;
    cout<<"String: "<<lcs_r.build_lcs_str ();

}
