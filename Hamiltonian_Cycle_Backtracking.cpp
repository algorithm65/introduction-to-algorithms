/***
Author: Maruf Ahmed
@https://gitlab.com/algorithm65/introduction-to-algorithms

Hamiltonian Cycle Problem using Backtracking

https://www.whitman.edu/mathematics/cgt_online/book/section05.03.html
*/

#ifndef Hamiltonian_Cycle_Backtrack_CPP
#define Hamiltonian_Cycle_Backtrack_CPP

#include <iostream>
#include <iomanip>

using namespace std;

class Hamiltonian_Cycle_Backtrack{

      long   no_of_vertex;
      float  **adj_matrix;
      int    *h_path;

      long   total_sol;
      long   call_count;

  public:
      Hamiltonian_Cycle_Backtrack(int v_no){
          this->call_count = 0;
          this->total_sol  = 0;

          this->no_of_vertex = v_no;

          adj_matrix = new float *[no_of_vertex];
          h_path     = new int    [no_of_vertex];


          for(int i=0; i<no_of_vertex; i++){
             h_path[i] = -1;

             adj_matrix[i] = new float [no_of_vertex];
             for(int j=0; j<no_of_vertex; j++)
                 adj_matrix[i][j] = 0.0;
          }
     }

      ~Hamiltonian_Cycle_Backtrack(){
          for(int i=0; i<no_of_vertex; i++)
             delete [] adj_matrix[i];
          delete [] adj_matrix;
          delete [] h_path;

     }

     int get_total_sol(){
         return total_sol;
     }

      void input(){

           int v1,v2;
           float weight;

           while(1){
              cin>>v1;
              if (v1 == -1) break;
              cin>>v2;
              //cin>>weight;
              weight = 1;
              if (v1==v2) continue;
              else if (adj_matrix[v1][v2]==0){
                    adj_matrix[v1][v2] = weight;
                    adj_matrix[v2][v1] = weight;
              }
              else if (adj_matrix[v1][v2]>weight){
                    adj_matrix[v1][v2] = weight;
                    adj_matrix[v2][v1] = weight;
              }
          }
      }

     bool is_connected(int u, int v){
         if (adj_matrix [u] [v] == 1)
              return true;
         return false;
     }
     bool is_repeat(int v, int index){
          for (int i = 0; i<index; i++)
              if(h_path[i] == v)
                  return  true;
           return false;
      }

     void start_path_search(){
          h_path[0] = 0;
          find_h_path(1);
     }

     void find_h_path( int index){

          //cout<<"call_count: "<<call_count++<<endl;
          if (index>no_of_vertex)
              return;
          else if (index==no_of_vertex &&
                    is_connected(h_path[index-1], 0)) {
               total_sol++;
               cout<<"solution no."<<total_sol<<endl;
               print_path();
               return ;
          }
          else
               for(int v=1; v<no_of_vertex; v++){
                   h_path[index] = v;
                   if (is_repeat(v, index)==false &&
                              is_connected ( h_path[index-1], v ) ==true)
                        find_h_path( index+1 );
                    h_path[index] = -1;
            }
      }

      void print_path(){
          cout<<"{";
          for (int i = 0; i< no_of_vertex ; i++){
               cout<<setw(3)<<h_path[i];
               if(i<no_of_vertex-1)cout<<",";
          }
          cout<<"}"<<endl;
      }

      void print_adj_matrix(){
          for (int i = 0; i<no_of_vertex; i++){
               for (int j = 0; j<no_of_vertex; j++){
                     cout<<adj_matrix[i][j];
                     j<no_of_vertex-1 ?  cout<<setw(4):  cout<<setw(0)<<endl; ;
               }
          }
          cout<<endl;
      }
};

#endif // Hamiltonian_Cycle_Backtrack_CPP
