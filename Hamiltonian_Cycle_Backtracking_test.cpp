/***
Author: Maruf Ahmed
@https://gitlab.com/algorithm65/introduction-to-algorithms

Hamiltonian Cycle Problem using Backtracking
Main file
*/

#include "Hamiltonian_Cycle_Backtracking.cpp"

int main(){
    int v,c;
    cout<<"********************\n";
    cout<<"Hamiltonian Cycle Problem using Backtracking.\n";
    cout<<"Input: enter the number of vertices\n";
    cout<<"followed by a <v1 v2> pair for each edge\n";
    cout<<"edge input terminated with an -1\n";
    cout<<"********************\n";

    cin>>v;
    cout<<"No of vertices:"<<v<<endl;
    Hamiltonian_Cycle_Backtrack  hc_bt (v) ;
    hc_bt.input();
    hc_bt.print_adj_matrix();
    cout<<"\n";
    hc_bt .start_path_search();
    cout<<"\n";
    cout<<"Total Solutions: "<< hc_bt .get_total_sol()<<endl;

 }
