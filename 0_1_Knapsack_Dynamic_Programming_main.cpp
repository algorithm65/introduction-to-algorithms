/***
0/1 Knapsack - Dynamic Programming
Main file
*/

#include "0_1_Knapsack_Dynamic_Programming.cpp"

int main(){
    int n,c;
    cout<<"********************\n";
    cout<<"0/1 Knapsack - Dynamic Programming.\n";
    cout<<"Input: First input the number of items and bin capacity\n";
    cout<<"Then values/profit for all the items \n";
    cout<<"Then weights for all the items item\n";
    cout<<"********************\n";

    cin>>n;
    cout<< "Total items: "<<n<<endl;
    cin>>c;
    cout<< "Total capacity: "<<c<<endl;
    Knapsack_DP knap_dp (n,c) ;
    knap_dp.input();
    knap_dp.print_max_matrix();
    knap_dp.find_max();
    knap_dp.print_max_matrix();
    cout<<"Result "<<endl;
    knap_dp .print_selected_items();


}
