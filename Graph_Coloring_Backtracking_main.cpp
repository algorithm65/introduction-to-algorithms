/***
Author: Maruf Ahmed
@https://gitlab.com/algorithm65/introduction-to-algorithms

Graph Coloring Problem using Backtracking
Main file
*/

#include "Graph_Coloring_Backtracking.cpp"

int main(){
    int v,c;
    cout<<"********************\n";
    cout<<"Graph Coloring Problem using Backtracking.\n";
    cout<<"Input: enter the number of colors and number of vertices\n";
    cout<<"followed by a <v1 v2> pair for each edge\n";
    cout<<"edge input terminated with an -1\n";
    cout<<"********************\n";

    cin>>c;
    cin>>v;
    cout<<"No of color(s):"<<c<<endl;
    Graph_Color_Backtrack  gc_bt (v,c) ;
    gc_bt.input();
    gc_bt.print_adj_matrix();
    cout<<"\n";
    gc_bt.find_color(0);
    cout<<"\n";
    cout<<"Total Solutions: "<< gc_bt.get_total_sol()<<endl;

 }
