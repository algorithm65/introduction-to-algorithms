/***
Dijkstra Algorithm for undirected graphs
Main file
*/

#include "Dijkstra_Algorithm.cpp"

int main(){
    int n;
    cout<<"********************\n";
    cout<<"Dijkstra Algorithm.\n";
    cout<<"Input: First input the number of nodes\n";
    cout<<"Followed by each edge in the form: v1 v2 weight\n";
    cout<<"Edge input stops with an -1\n";
    cout<<"********************\n";

    cin>>n;
    cout<< "Total Nodes: "<<n<<endl;
    Dijkstra_SPF d_spf (n) ;
    d_spf.input();
    d_spf.print_adj_matrix();
    d_spf.find_SPF(0);
    cout<<"Vertex Distances"<<endl;
    d_spf.print_vertex_SP();

}
