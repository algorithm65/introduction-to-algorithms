/***
Kruskals algorithm to find MST
*/

#ifndef Kruskals_MST_CPP
#define Kruskals_MST_CPP

#include <iostream>
#include <iomanip>
#include <set>
#include <queue>
#include "disjoint_set.cpp"

using namespace std;

class Kruskals_MST{

      int no_of_vertices;
      float **adj_matrix;
      priority_queue< pair<int, pair<int,int> > > vertex_pq;
      priority_queue< pair<int, int> >  MST_edges;

  public:
      Kruskals_MST(int n){
          this->no_of_vertices = n;

          adj_matrix = new float *[n];
          for(int i=0; i<n; i++){
             adj_matrix[i] = new float [n];
             for(int j=0; j<n; j++)
                 adj_matrix[i][j] = 0;
          }
      }

      ~Kruskals_MST(){
         for(int i=0; i<this->no_of_vertices; i++)
             delete [] adj_matrix[i];
          delete [] adj_matrix;
     }

      void input(){

           int v1,v2;
           float weight;

           while(1){
              cin>>v1;
              if (v1 == -1) break;
              cin>>v2>>weight;

              if (v1==v2) continue;
              else if (adj_matrix[v1][v2]==0){
                    adj_matrix[v1][v2] = weight;
                    adj_matrix[v2][v1] = weight;
              }
              else if (adj_matrix[v1][v2]>weight){
                    adj_matrix[v1][v2] = weight;
                    adj_matrix[v2][v1] = weight;
              }
          }
      }

      void find_mst(){

           disjoint_set  d_s(no_of_vertices);

           for(int i=0; i<no_of_vertices; i++)
               for(int j=i+1; j<no_of_vertices; j++)
                    if(adj_matrix[i][j] != 0)
                        vertex_pq.push( make_pair( -adj_matrix[i][j], make_pair( i,j) ) );

           while(!vertex_pq.empty()){
               pair<int, pair <int,int> > p1 = vertex_pq.top();
               pair <int,int> p2 = p1.second;
               vertex_pq.pop();

               int v1 = p2.first;
               int v2 = p2.second;
               int v1_parent = d_s.find_parent(v1);
               int v2_parent = d_s.find_parent(v2);

               if(v1_parent != v2_parent){
                   //cout <<v1<<"  -  "<<v2<<endl;
                   MST_edges.push( make_pair(-v1,v2) );
                   d_s.sets_union (v1, v2);
               }
           }

      }

        void print_vertex_edge(){
          int cost = 0;
          cout<<"v1"<<setw(5)<<"v2"<<setw(8)<<"weight"<<setw(0)<<endl;
          while(!MST_edges.empty()){
                pair<int, int> temp = MST_edges.top();
                int v1 = -temp.first;
                int v2 = temp.second;
                MST_edges.pop();
                v1 < v2 ?  cout <<v1<<"  -  "<<v2 : cout <<v2<<"  -  "<<v1;;
                     cout <<setw(5)<<adj_matrix[v1][v2]<<setw(0)<<endl ;
                     cost += adj_matrix[v1][v2];
          }
          cout<<endl;
          cout<<"Minimum cost: "<<cost<<endl;
      }

      void print_adj_matrix(){
          for (int i = 0; i<no_of_vertices; i++){
               for (int j = 0; j<no_of_vertices; j++){
                     cout<<adj_matrix[i][j];
                     j<no_of_vertices-1 ?  cout<<setw(4):  cout<<setw(0)<<endl; ;
               }
          }
         cout<<endl;
      }
};

#endif // Prims_MST_CPP

