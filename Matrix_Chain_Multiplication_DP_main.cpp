/***
Matrix Chain Multiplication using Dynamic Programming
Main file
*/

#include "Matrix_Chain_Multiplication_DP.cpp"

int main(){
    int n;
    cout<<"********************\n";
    cout<<"Matrix Chain Multiplication using Dynamic Programming.\n";
    cout<<"Input: First input the number of Matrices\n";
    cout<<"Followed by the dimensions \n";
    cout<<"********************\n";

    cin>>n;
    cout<< "Total Nodes: "<<n<<endl;
    Matrix_Chain_DP mc_dp (n) ;
    mc_dp.input();
    mc_dp.print_dimension();
    mc_dp.find_order();

    cout<<"Minimum cost - matrix"<<endl;
    mc_dp.print_M_matrix();
    cout<<"Subsequent path - matrix"<<endl;
    mc_dp.print_S_matrix();
    cout<<"Optimal chain multiplication"<<endl;
    mc_dp.print_optimal (0, 3);


}
