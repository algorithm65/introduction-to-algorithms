/***
Two way array merge
*/

#include <iostream>
#include "merge_twoway.cpp"

using namespace std;

int main(){

    int *list1, *list2, *merged_list ;
    int  m,n;
    int value;

    cout<<"\nTotal elements in the first list, followed by the elements\n";
    cin>>m;
    list1 = new int [m];
    for (int i=0; i<m; ){
        cin>>value;
        cout<<value<<" ";
        list1[i++] =value;
    }

    cout<<"\nTotal elements in the second list, followed by the elements\n";
    cin>>n;
    list2 = new int [n];
    for (int j=0; j<n; ){
        cin>>value;
        cout<<value<<" ";
        list2[j++] =value;
    }
    cout<<"\nMerged list\n";
    twoway_merge t_m(list1, list2, m,n);
    merged_list = t_m.merge_list();
    for (int k=0; k<n+m; ){
        value=merged_list[k++];
        cout<<value<<" ";
    }
}
