/***
MultiStage Graph Dynamic Programming
main function
*/

#include "MultiStage_Graph_Dynamic_Programming.cpp"

int main(){
    int n, stages;
    cout<<"********************\n";
    cout<<"MultiStage Graph - Dynamic Programming.\n";
    cout<<"Input: First input the number of nodes and stages\n";
    cout<<"Followed by each edge in the form: v1 v2 weight \n";
    cout<<"Edge input stops with -1\n";
    cout<<"********************\n";

    cin>>n;
    cin>>stages;
    cout<< "Total Nodes: "<<n<<endl;
    MultiStage_DP  ms_dp (n, stages) ;

    ms_dp.input_edges();
    ms_dp.print_adj_matrix();
    ms_dp.shortest_path();
    ms_dp.print_arrays();
    cout<<setw(5)<<"Path:";
    ms_dp.print_path();


}
