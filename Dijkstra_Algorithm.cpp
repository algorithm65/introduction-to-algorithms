/***
Dijkstra Algorithm for undirected graphs
*/

#ifndef Dijkstra_Algorithm_CPP
#define Dijkstra_Algorithm_CPP

#include <iostream>
#include <iomanip>
#include <set>
#include <queue>
#include <vector>
#include <algorithm>

using namespace std;

class Dijkstra_SPF{

      int no_of_vertices;
      float **adj_matrix;
      float  *dist;
      int    *prev;

      vector <int> vertex_set;
      priority_queue< pair<int, int> >  selected_edges;

  public:
      Dijkstra_SPF(int n){
          this->no_of_vertices = n;

          adj_matrix = new float *[n];
          dist = new float [n];
          prev = new int [n];

          for(int i=0; i<n; i++){
             dist[i] = INT_MAX;
             prev[i] = -1;

             adj_matrix[i] = new float [n];
             for(int j=0; j<n; j++)
                 adj_matrix[i][j] = 0;
          }
     }

      ~Dijkstra_SPF(){
         for(int i=0; i<this->no_of_vertices; i++)
             delete [] adj_matrix[i];
          delete [] adj_matrix;
          delete [] dist;
          delete [] prev;
     }

      void input(){

           int v1,v2;
           float weight;

           while(1){
              cin>>v1;
              if (v1 == -1) break;
              cin>>v2>>weight;

              if (v1==v2) continue;
              else if (adj_matrix[v1][v2]==0){
                    adj_matrix[v1][v2] = weight;
                    adj_matrix[v2][v1] = weight;
              }
              else if (adj_matrix[v1][v2]>weight){
                    adj_matrix[v1][v2] = weight;
                    adj_matrix[v2][v1] = weight;
              }
          }
      }

      void find_SPF(int source){

           dist[source] = 0;

           for(int i=0; i<no_of_vertices; i++)
                       vertex_set.push_back( i ) ;

           while(!vertex_set.empty()){
               int min_v = vertex_set[0];
               int min_value = dist[min_v];
               int min_pos   = 0;
               for (int v=0; v< vertex_set.size(); v++)
                   if(dist[vertex_set[v]]<min_value){
                        min_value = dist[vertex_set[v]];
                        min_v = vertex_set[v];
                        min_pos  = v;
                   }

               vertex_set.erase( vertex_set.begin()+min_pos );

               int u = min_v;
               for(int v=0; v<no_of_vertices; v++)
                   if(adj_matrix[u][v] != 0 &&
                        dist[v] >  dist[u] + adj_matrix[u][v]){
                            dist[v] = dist[u] + adj_matrix[u][v] ;
                            prev[v] = u;
                   }
            }
       }

        void print_vertex_SP(){

          cout<<"V"<<setw(10)<<"Dist."<<setw(10)<<"Prev. v"<<endl;
          for(int v=0; v<no_of_vertices; v++)
               cout <<v<<setw(8)<<dist[v]<<setw(8)<<prev[v]<<endl;

      }

      void print_adj_matrix(){
          for (int i = 0; i<no_of_vertices; i++){
               for (int j = 0; j<no_of_vertices; j++){
                     cout<<adj_matrix[i][j];
                     j<no_of_vertices-1 ?  cout<<setw(4):  cout<<setw(0)<<endl; ;
               }
          }
         cout<<endl;
      }
};

#endif // Dijkstra_Algorithm


