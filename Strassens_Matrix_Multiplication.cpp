/***

Strassen's Matrix Multiplication

***/

#ifndef Strassens_Matrix_Multiplication_h
#define Strassens_Matrix_Multiplication_h

#include <iostream>
#include "matrix_grid.cpp"
#include "matrix_operations.cpp"

 class Strassen{

    public:

    Strassen(){
    }

    matrix Strassen_MM(matrix t1, matrix t2){

         matrix a(t1);
         matrix b(t2);

         if (a.get_m_size()==1){
              matrix c = a*b;
              return c;
         }
         else{

              matrix_grid  a_split(a, 2);
              matrix_grid  b_split(b, 2);
              matrix_grid  c_split(a_split.get_size_g(),  a_split.get_sub_size(), 0);

              //a_split.print_m(0,0);
              //a_split.print_m(0,1);

              matrix P1 = Strassen_MM(a_split.get_m(0,0), b_split.get_m(0,1) - b_split.get_m(1,1));
              matrix P2 = Strassen_MM(a_split.get_m(0,0) + a_split.get_m(0,1), b_split.get_m(1,1));
              matrix P3 = Strassen_MM(a_split.get_m(1,0) + a_split.get_m(1,1), b_split.get_m(0,0));
              matrix P4 = Strassen_MM(a_split.get_m(1,1), b_split.get_m(1,0) - b_split.get_m(0,0));
              matrix P5 = Strassen_MM(a_split.get_m(0,0) + a_split.get_m(1,1), b_split.get_m(0,0) + b_split.get_m(1,1));
              matrix P6 = Strassen_MM(a_split.get_m(0,1) - a_split.get_m(1,1), b_split.get_m(1,0) + b_split.get_m(1,1));
              matrix P7 = Strassen_MM(a_split.get_m(0,0) - a_split.get_m(1,0), b_split.get_m(0,0) + b_split.get_m(0,1));

              matrix C11  = P5 + P4 - P2 + P6;
              matrix C12  = P1 + P2;
              matrix C21  = P3 + P4;
              matrix C22  = P1 + P5 - P3 - P7;

              c_split.add_m(0, 0, C11);
              c_split.add_m(0, 1, C12);
              c_split.add_m(1, 0, C21);
              c_split.add_m(1, 1, C22);

              matrix ret_m = c_split.join_m();
              return ret_m;
          }
      }
 };
 #endif
