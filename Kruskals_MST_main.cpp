/***
Kruskal's algorithm to find MST
Main file
*/

#include "Kruskals_MST.cpp"

int main(){
    int n;
    cout<<"********************\n";
    cout<<"Kruskal's MST algorithm.\n";
    cout<<"Input: First input the number of nodes\n";
    cout<<"Followed by each edge in the form: v1 v2 weight \n";
    cout<<"Edge input stops with -1\n";
    cout<<"********************\n";

    cin>>n;
    cout<< "Total Nodes: "<<n<<endl;
    Kruskals_MST k_mst (n) ;
    k_mst.input();
    k_mst.print_adj_matrix();
    k_mst.find_mst();
    //p_mst.print_MST_vertex();
    cout<<"Selected Edges"<<endl;
    k_mst.print_vertex_edge();


}
