/***
Knuth-Morris-Pratt (KMP) String Matching Algorithm

More detail at:
https://www-igm.univ-mlv.fr/~lecroq/string/node8.html
*/

#ifndef Knuth_Morris_Pratt
#define Knuth_Morris_Pratt

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>

using namespace std;

class Knuth_Morris_Pratt_Algorithm{

      string  str1;
      string  pattern_str;
      int     prefix_len;
      int     str_len, pattern_len;
      int     *prefix_array;
      vector  <int> result;
      int     call_count;

  public:
    Knuth_Morris_Pratt_Algorithm(string str1, string  pattern ){
        this->str1 = str1;
        this->pattern_str = pattern;

        str_len = str1.size();
        pattern_len = pattern.size();
        prefix_array = new int [pattern_len];
        call_count   = 0;
    }

    void build_prefix_array(){

         int i=1;
         prefix_len = 0;
         prefix_array[0] =0;

         while (i < pattern_str.size()) {
            if(pattern_str.at(i)==pattern_str.at(prefix_len)){
                prefix_len++;
                prefix_array [i] = prefix_len;
                i++;
            }
            else if (pattern_str.at(i)!=pattern_str.at(prefix_len)
                      && prefix_len !=0 ){
                      prefix_len = prefix_array [prefix_len-1];

                     }
             else if (pattern_str.at(i)!=pattern_str.at(prefix_len)
                      && prefix_len ==0 ){
                         prefix_array[i] = 0;
                         i++;
                      }
            }
     }

     void search_str(){

          int i,j;
          i= j = 0;
          build_prefix_array();

          while (i<str1.size()){
               //cout<<call_count++<<endl;
               if(str1.at(i) == pattern_str.at(j)){
                       i++; j++;
               }
               if(j==pattern_str.size()){
                    result.push_back(i-j);
                    j = prefix_array[j-1];
               }
               else if (i<str1.size() &&  str1.at(i) != pattern_str.at(j)  &&
                        j != 0){
                            j = prefix_array[j-1];
                        }
                else if (i<str1.size() && str1.at(i) != pattern_str.at(j) &&
                        j == 0){
                            i++;
                        }
          }
     }

     void print_prefix_array(){
         for(int i=0; i<pattern_str.size(); i++)
             cout<<setw(4)<<prefix_array[i];
         cout<<endl;
     }

     void print_results(){
         for(int i=0; i<result.size(); i++)
             cout<<setw(4)<<result[i];
         cout<<endl;
     }
};

#endif //Knuth_Morris_Pratt
