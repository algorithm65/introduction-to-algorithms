/***
Traveling Salesman Problem - Branch and Bound

More details:
https://www.techiedelight.com/travelling-salesman-problem-using-branch-and-bound/
**/

#ifndef Traveling_Salesman_Branch_and_Bound
#define Traveling_Salesman_Branch_and_Bound

#include <iostream>
#include <iomanip>
#include <queue>
#include <set>
#include <algorithm>

using namespace std;



class node{
  public:
     vector<int>  nodes_set;
     int       v_index;
     float     node_cost;
     long      node_no;
     int       matrix_size;
     float   **node_matrix;

     node (int n){
        v_index  = -1;
        node_cost  = 0;

        matrix_size = n;
        node_matrix = new float * [n];
        for(int i=0; i< matrix_size; i++)
            node_matrix[i] = new float [n];
     }
     void set_node_matrix(float **adj_matrix){
          for(int i=0; i< matrix_size; i++)
            for (int j=0; j< matrix_size; j++)
                node_matrix[i][j] = adj_matrix[i][j];

     }
     void set_node_edge(int u, int v){
          for(int j=0; j< matrix_size; j++)
               node_matrix[u][j] = INT_MAX;
          for(int i=0; i< matrix_size; i++)
               node_matrix[i][v] = INT_MAX;

          node_matrix[v][u] = INT_MAX;
     }
     ~node(){
           for(int i=0; i< matrix_size; i++)
               delete node_matrix[i];
           delete node_matrix;
     }
     friend bool operator<(const node &a, const node &b);

};

bool operator<(const node &a, const node &b){
     return a.node_cost > b.node_cost;
}

class Traveling_Salesman_BB{

      long    no_of_vertex;
      float   global_upper;
      long    itr_count;
      int     node_count;
      long    global_min;
      float  **adj_matrix;

      priority_queue < node > nodes_queue;
      vector<int> min_set;

  public:
      Traveling_Salesman_BB (int n){
          this->itr_count = 0;
          this->node_count = 0;
          this->no_of_vertex = n;
          this->global_min = INT_MAX;

          adj_matrix = new float *[no_of_vertex];
          for(int i=0; i<no_of_vertex; i++){
               adj_matrix[i] = new float [no_of_vertex];
               for(int j=0; j<no_of_vertex; j++)
                     adj_matrix[i][j] = INT_MAX;
          }
      }

      ~Traveling_Salesman_BB(){
          for(int i=0; i<no_of_vertex; i++)
               delete [] adj_matrix[i];
          delete [] adj_matrix;
     }

      void input(){

           int v1,v2;
           float weight;

           while(1){
              cin>>v1;
              if (v1 == -1) break;
              cin>>v2;
              cin>>weight;

              if (v1==v2) continue;
              else if (adj_matrix[v1][v2]>weight)
                    adj_matrix[v1][v2] = weight;
          }
      }

     vector<int> get_min_set(){
         return min_set;
     }
     long get_min_cost(){
         return global_min;
     }
     float** get_adj_matrix(){
         return adj_matrix;
     }

     float** matrix_reduction (float **adj, float* c){
          float cost;

          for(int i=0; i<no_of_vertex; i++){
               int min_row = INT_MAX;
               for(int j=0; j<no_of_vertex; j++)
                      if(adj[i][j]< min_row) min_row = adj[i][j];
               if( min_row >= INT_MAX) min_row = 0;
               if ( min_row == 0) continue;
               cost += min_row;
               for(int j=0; j<no_of_vertex; j++)
                      adj[i][j] = (adj[i][j]< INT_MAX)? adj[i][j] - min_row: INT_MAX ;
          }

          for(int j=0; j<no_of_vertex; j++){
               int min_col = INT_MAX;
               for(int i=0; i<no_of_vertex; i++)
                      if(adj[i][j]< min_col) min_col = adj[i][j];
               if( min_col >= INT_MAX) min_col = 0;
               if ( min_col == 0) continue;
               cost +=min_col;
               for(int i=0; i<no_of_vertex; i++)
                      adj[i][j] = (adj[i][j]< INT_MAX)? adj[i][j] - min_col: INT_MAX ;
          }
         *c = cost;
          return adj;
     }

     void set_edge(int n, float **adj, int u, int v){
          for(int j=0; j< n; j++)
               adj[u][j] = INT_MAX;
          for(int i=0; i< n; i++)
               adj[i][v] = INT_MAX;

          adj[v][u] = INT_MAX;
     }

     void start_search(){

          node_count = 1;
          global_upper = INT_MAX;

          node first_node(no_of_vertex);
          first_node.v_index = 0;
          first_node.node_no = node_count;
          first_node.nodes_set.push_back(0);

          matrix_reduction (adj_matrix,&(first_node.node_cost));
          first_node.set_node_matrix(adj_matrix);
          nodes_queue.push(first_node);


          while(!nodes_queue.empty()){

               node temp =  nodes_queue.top();
               node front_node(no_of_vertex);
               front_node.v_index   = temp.v_index  ;
               front_node.node_no   = temp.node_no;
               front_node.nodes_set = temp.nodes_set;
               front_node.node_cost = temp.node_cost ;
               front_node.set_node_matrix(temp.node_matrix);

               nodes_queue.pop();

               for(int i = 0; i<no_of_vertex; i++)
                    if (front_node.node_matrix[front_node.v_index] [i] < INT_MAX) {

                        node_count ++;
                        if(front_node.v_index == no_of_vertex -1){
                              global_upper =  front_node.node_cost;
                              if(global_min > front_node.node_cost)  {
                                    global_min = front_node.node_cost;
                                    min_set    = front_node.nodes_set;
                              }
                        }

                        node new_node(no_of_vertex);
                        new_node.v_index = i;
                        new_node.node_no = node_count;
                        new_node.set_node_matrix(front_node.node_matrix);
                        new_node.nodes_set  = front_node.nodes_set;
                        new_node.nodes_set.push_back(i);

                        new_node.set_node_edge(front_node.v_index, i);

                        float c = 0;
                        matrix_reduction (new_node.node_matrix,  &c);

                        new_node.node_cost =  front_node.node_matrix[front_node.v_index] [i]
                                              + front_node.node_cost
                                              + c;

                        if(new_node.node_cost < global_upper)
                              nodes_queue.push(new_node);
               }
          }
     }

      void print_adj_matrix(int n, float **adj){
          for (int i = 0; i<n; i++){
               for (int j = 0; j<n; j++)
                     adj[i][j] < INT_MAX  ?  cout<<setw(5)<<adj[i][j]:  cout<<setw(5)<<"INF";
               cout<<endl;
          }
      }
      void print_set(vector<int> nodes_set){
         cout<<" { ";
         for (int i=0; i<nodes_set.size(); ++i){
                 cout<<nodes_set[i];
                 if(i < nodes_set.size()-1) cout<<"-> ";
         }
         cout<<" } ";
     }
  };
#endif // Traveling_Salesman_Branch_and_Bound


