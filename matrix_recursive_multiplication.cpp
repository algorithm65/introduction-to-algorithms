/***

Recursive Matrix multiplication
For more details:
https://www.cs.mcgill.ca/~pnguyen/251F09/matrix-mult.pdf
***/

#ifndef matrix_recursive_multiplication_h
#define matrix_recursive_multiplication_h

#include <iostream>
#include "matrix_grid.cpp"
#include "matrix_operations.cpp"

 class mm_recursive{

    public:

     mm_recursive(){
     }

     matrix mm_re(matrix a, matrix b){

          if (a.get_m_size()==1){
              matrix c = a*b;
              return c;
          }
          else{
              matrix_grid  a_split(a, 2);
              matrix_grid  b_split(b, 2);
              matrix_grid  c_split(a_split.get_size_g(),  a_split.get_sub_size(), 0);

              for (int i=0; i<a_split.get_size_g() ; i++)
                 for (int j=0; j<a_split.get_size_g() ; j++)
                     for (int k=0; k<a_split.get_size_g() ; k++)
                           c_split.add_m(i, j, (a_split.get_m(i,k) * b_split.get_m(k,j)));

              matrix ret_m = c_split.join_m();
              return ret_m;
          }
      }
 };

 #endif
