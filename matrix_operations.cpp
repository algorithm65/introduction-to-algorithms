/***
Basic square Matrix manipulations
Operator overloading
*/
#ifndef matrix_operations_h
#define matrix_operations_h

#include <iostream>
#include <stdlib.h>

using namespace std;

class matrix {

     int **m_array;
     int  m_size;

     public:
     matrix();
     matrix(int m_size, int init);
     matrix(const matrix & b);

     int get_m_size ();
     int get_m_array(int i,int j);
     void set_m_array(int i,int j, int value);

     void input_m();
     void print_m();

     matrix operator + ( const matrix & b );
     matrix& operator += ( const matrix & b );
     matrix operator - ( const matrix & b );
     matrix operator * ( const matrix & b );
     matrix& operator=( const matrix & b );
     ~matrix();
 };


     matrix:: matrix(){
     }

     matrix:: ~matrix(){
       for (int i=0; i<m_size; i++)
            delete[] m_array[i];
       delete[] m_array;
     }

     matrix:: matrix(int m_size, int init){

        this -> m_size = m_size;
        m_array = new int * [m_size];
        for (int i=0; i<m_size; i++)
            m_array[i] = new int [m_size];

        for (int i=0; i<m_size; i++)
            for (int j=0; j<m_size; j++)
                 m_array[i][j] = init;
     }

     matrix:: matrix(const matrix & b){
        this -> m_size = b.m_size;
        m_array = new int * [m_size];
        for (int i=0; i<m_size; i++)
            m_array[i] = new int [m_size];

        for (int i=0; i<m_size; i++)
            for (int j=0; j<m_size; j++)
                 m_array[i][j] =  b.m_array[i][j];
     }

     int  matrix:: get_m_size (){
         return m_size;
     }
     int  matrix:: get_m_array(int i,int j){
         if (i<m_size && j<m_size)
              return m_array[i][j];
     }
     void  matrix:: set_m_array(int i,int j, int value){
         if (i<m_size && j<m_size)
              m_array[i][j] = value;
     }
     void  matrix:: input_m(){
        for (int i=0;i<m_size ; i++)
              for (int j=0; j<m_size; j++)
                   cin>>m_array[i][j];
     }

     void  matrix:: print_m(){
          cout<<endl;
          for (int i=0;i<m_size ; i++){
              for (int j=0; j<m_size; j++)
                   cout<<m_array[i][j]<<" ";
              cout<<endl;
          }
     }

     matrix  matrix:: operator + ( const matrix & b ){
          if (m_size != b.m_size){
              cout<<"\noperator + : Matrix sizes do not match";
              return matrix(m_size, -1);
          }
          matrix c(m_size,0);
          for (int i=0;i<m_size ; i++)
              for (int j=0; j<m_size; j++)
                   c.m_array[i][j] = m_array[i][j] + b.m_array[i][j];
          return c;
      }

      matrix&  matrix:: operator += ( const matrix & b ){
          if (m_size != b.m_size){
              cout<<"\noperator += : Matrix sizes do not match";
              return *this;
          }
          for (int i=0;i<m_size ; i++)
              for (int j=0; j<m_size; j++)
                   m_array[i][j] = m_array[i][j] + b.m_array[i][j];
          return *this;
      }

      matrix  matrix:: operator - ( const matrix & b ){
          if (m_size != b.m_size){
              cout<<"\noperator - : Matrix sizes do not match";
              return matrix(m_size, -1);
          }
          matrix c(m_size,0);
          for (int i=0;i<m_size ; i++)
              for (int j=0; j<m_size; j++)
                   c.m_array[i][j] = m_array[i][j] - b.m_array[i][j];
          return c;
      }

      matrix  matrix:: operator * ( const matrix & b ){
          if (m_size != b.m_size){
              cout<<"\noperator * : Matrix sizes do not match";
              return matrix(m_size, -1);
          }
          matrix c(m_size,0);
          for (int i=0;i<m_size ; i++)
              for (int j=0; j<m_size ; j++)
                   for(int k=0; k<m_size ; k++)
                         c.m_array[i][j] += m_array[i][k] * b.m_array[k][j];
          return c;
      }

      matrix&  matrix:: operator=( const matrix & b ){

          this->m_size = b.m_size;
          m_array = new int * [m_size];
          for (int i=0; i<m_size; i++)
               m_array[i] = new int [m_size];

          for (int i=0;i<b.m_size ; i++)
              for (int j=0; j<b.m_size; j++)
                   this->m_array[i][j] = b.m_array[i][j];

          return *this;
      }

#endif // matrix_operations_h


