/***
Matrix Chain Multiplication using Dynamic Programming

More details at: https://www.radford.edu/~nokie/classes/360/dp-matrix-parens.html
*/

#ifndef Matrix_Chain_Multiplication_DP
#define Matrix_Chain_Multiplication_DP
#include <iostream>
#include <iomanip>


using namespace std;

class Matrix_Chain_DP{

      int no_of_matrices;
      int *d;
      int **M;
      int **S;

      public:
      Matrix_Chain_DP (int n){
          this->no_of_matrices = n;
          d = new int  [n+1];
          M = new int *[n];
          S = new int *[n];
          for(int i=0; i<n; i++){
             d [i] = 0;
             M[i] = new int [n];
             S[i] = new int [n];
             for(int j=0; j<n; j++){
                 M[i][j] = -1;
                 S[i][j] = -1;
             }
          }
      }

      ~Matrix_Chain_DP(){
         for(int i=0; i<no_of_matrices; i++){
             delete [] M[i];
             delete [] S[i];
         }
          delete [] M;
          delete [] S;
          delete [] d;
     }

      void input(){
         for(int i=0; i<=no_of_matrices; i++)
              cin>>d [i] ;
      }

      void find_order(){
          for(int i=0; i<no_of_matrices; i++)
                M [i] [i] = 0;

           for(int diff=1; diff<no_of_matrices; diff++){
               for(int i=0; i<no_of_matrices - diff ; i++){
                    int j = i + diff;
                    M [i] [j] = INT_MAX;
                    //cout<<"i :"<<i<<" j:"<<j<<", "<<endl;
                    for (int k = i ; k < j  ; k++){
                       int cost = M[i][k] + M[k+1][j] + d[i]* d[k+1] * d[j+1];
                       //cout<<"k :"<<k<<" cost:"<<cost<<", ";
                       if(cost < M[i][j]){
                            M[i] [j] = cost;
                            S[i] [j] = k;
                        }
                    }
                    //cout<<endl;
               }
               //cout<<endl;
           }
       }

      void print_dimension(){
          for (int i = 0; i<no_of_matrices+1; i++)
               cout<<setw(5)<<"d["<<i<<"]";
          cout<<endl;
          for (int i = 0; i<no_of_matrices+1; i++)
               cout<<setw(7)<<d[i];
          cout<<endl;
      }

      void print_M_matrix(){
          for (int i = 0; i<no_of_matrices; i++){
               for (int j = 0; j<no_of_matrices; j++){
                     if (M[i] [j] == -1) cout<<setw(6)<<"x";
                     else if (M[i][j] >=32700) cout<<setw(6)<<"INF";
                     else cout<<setw(6)<<M[i][j];
                }
                cout<<endl;
          }
          cout<<endl;
      }

      void print_S_matrix(){
          for (int i = 0; i<no_of_matrices; i++){
               for (int j = 0; j<no_of_matrices; j++){
                     if (S[i] [j] == -1) cout<<setw(6)<<"x";
                     else cout<<setw(6)<<S[i][j];
                }
                cout<<endl;
          }
          cout<<endl;
      }

      void print_optimal (int i, int j){
           if (i==j)
              cout<<"A"<<i<<". ";
           else {
              cout<<"(";
              print_optimal(i, S[i][j]);
              print_optimal(S[i][j]+1, j);
              cout<<")";
           }
      }
};

#endif // Matrix_Chain_Multiplication_DP

