/***
Huffman coding algorithm
using inverse tree
The main function
****/

#include "Huffman_coding.cpp"

int main(){
    huffman_tree h_t_1;
    int no;

    cout<<"Enter the number of elements, followed by characters and frequencies\n";

    cin>>no;
    cout<<endl<<"Number of elements:"<<no<<endl;
    h_t_1.input(no);
    cout<<endl;
    h_t_1.build_tree();
    //h_t_1.print_tree();
    cout<<"Coding\n";
    h_t_1.traverse_tree( h_t_1.get_global_serial() - 1, "");


    huffman_tree h_t_2;
    cin>>no;
    cout<<endl<<"Number of elements:"<<no<<endl;
    h_t_2.input(no);
    cout<<endl;
    h_t_2.build_tree();
    cout<<"Coding\n";
    h_t_2.traverse_tree( h_t_2.get_global_serial() - 1, "");

    huffman_tree h_t_3;
    cin>>no;
    cout<<endl<<"Number of elements:"<<no<<endl;
    h_t_3.input(no);
    cout<<endl;
    h_t_3.build_tree();
    cout<<"Coding\n";
    h_t_3.traverse_tree( h_t_3.get_global_serial() - 1, "");

    huffman_tree h_t_4;
    cin>>no;
    cout<<endl<<"Number of elements:"<<no<<endl;
    h_t_4.input(no);
    cout<<endl;
    h_t_4.build_tree();
    cout<<"Coding\n";
    h_t_4.traverse_tree( h_t_4.get_global_serial() - 1, "");



}


