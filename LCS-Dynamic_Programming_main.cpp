/***
Longest Common Subsequence (LCS) -Dynamic Programming
Main file
*/

#include "LCS-Dynamic_Programming.cpp"

int main(){
    string str1,str2;
    cout<<"********************\n";
    cout<<"Longest Common Subsequence (LCS) -Dynamic Programming.\n";
    cout<<"Input: Input two strings\n";
    cout<<"********************\n";

    cin>>str1;
    cin>>str2;

    LCS_DP lcs_dp (str1,str2) ;

    lcs_dp.find_lcs();
    cout<<endl;
    lcs_dp.print_L_matrix();
    cout<<"LCS: ";
    cout<<lcs_dp.print_lcs (str1.size(), str2.size());
    cout<<endl;

}
