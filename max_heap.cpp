/**
Max heap and heap sort
*/

#include <iostream>

using namespace std;

class max_heap{

   int *heap_array;
   int a_size;
   int heap_size;

   public:
   max_heap(int a_size){
       this->a_size = a_size;
       this->heap_size = 0;
       heap_array = new int [a_size];
       for (int i=0; i<a_size; i++ )
          heap_array[i] = 0;
   }
   void print_heap(){
       for (int i=0; i<a_size; i++ )
          cout<<heap_array[i]<<" ";
   }

   inline int left_child(int index){
        return index*2+1;
    }

    inline int right_child(int index){
        return index*2+2;
    }

    inline int parent(int index){
        return (index-1)/2;
    }

    void heap_insert(int value){

         if (heap_size == a_size){
            cout<<"Heap is full";
            return;
         }
         heap_array [heap_size] = value;
         int index = heap_size;
         while( index >=0){
             int parent_index = parent( index);

             if ( heap_array[ index] > heap_array[parent( index)]){
                  int temp = heap_array [ index];
                  heap_array [ index] = heap_array[parent( index)];
                  heap_array[parent( index)] = temp;
                  index = parent( index);
             }
             else
                break;
         }
         heap_size++;
    }

    int heap_extract(){
        int max_value = heap_array[0];
        heap_array[0] = heap_array[heap_size-1];

        int index = 0;
        heap_size--;
        heap_array[heap_size]=0;
        while (index < heap_size-1){
          if (heap_array [index]<heap_array[ left_child(index) ] &&
                    heap_array[ left_child(index) ] > heap_array[ right_child(index) ] &&
                    left_child(index)<=heap_size){
               int temp = heap_array [index];
               heap_array [index] =  heap_array[ left_child(index) ];
               heap_array[ left_child(index) ] = temp;
               index = left_child(index);
           }
           else if(heap_array [index]<heap_array[ right_child(index) ]&&
                   right_child(index)<=heap_size){
               int temp = heap_array [index];
               heap_array [index] =  heap_array[ right_child(index) ];
               heap_array[ right_child(index) ] = temp;
               index = right_child(index);
           }
           else
              break;
        }

        return max_value ;
    }

    void heap_sort(){
        int value;
        for (int i=0; i<a_size;i++){
           cin>>value;
           cout<<value<<" ";
           heap_insert(value);
        }
        cout<<endl;
        cout<<"Heap:"<<endl;
        print_heap();
        cout<<endl;
        cout<<"sorted values:"<<endl;
        for (int i=0; i<a_size;i++){
           value = heap_extract();
           cout<<value<<" ";
        }
    }
};

int main(){
    int a_size;
    cout<< "Enter Heap size, followed by the values\n";
    cin>>a_size;
    max_heap  m_a(a_size);
    m_a.heap_sort();

}
