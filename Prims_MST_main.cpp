/***
Prim's algorithm to find MST
Main file
*/

#include "Prims_MST.cpp"

int main(){
    int n;
    cout<<"********************\n";
    cout<<"Prims MST algorithm.\n";
    cout<<"Input: First input the number of nodes\n";
    cout<<"Followed by each edge in the form: v1 v2 weight \n";
    cout<<"Edge input stops with -1\n";
    cout<<"********************\n";

    cin>>n;
    cout<< "Total Nodes: "<<n<<endl;
    Prims_MST  p_mst (n) ;
    p_mst.input();
    p_mst.print_adj_matrix();
    p_mst.find_mst(0);
    //p_mst.print_MST_vertex();
    cout<<"Selected Edges"<<endl;
    p_mst.print_vertex_edge();


}
