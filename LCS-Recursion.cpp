/***
Longest Common Subsequence (LCS) - Recursion

More detail at:
https://www.ics.uci.edu/~eppstein/161/960229.html
*/

#ifndef LCS_Recursion_CPP
#define LCS_Recursion_CPP

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>

using namespace std;

class LCS_Recursion{

      string    str1, str2, lcs_str;
      int       max_l;
      int       **L;
      vector    < pair<int,char> > lcs_vector;

  public:

      LCS_Recursion(string str1, string  str2){
          this-> str1 =  str1;
          this-> str2 =  str2;

          L = new int *[str1.length()+1];
          for(int i=0; i<= str1.length(); i++){
             L[i] = new int [ str2.length()+1];
             for(int j=0; j<= str2.length(); j++)
                 L[i][j] = -1;
          }
          lcs_str ="";
      }

      ~LCS_Recursion(){
         for(int i=0; i<=str1.length(); i++)
             delete [] L[i];
         delete [] L;
     }

     string get_lcs_str(){
         return lcs_str;
     }


     int find_lcs(int i, int j){

        if (L[i] [j] <0){
            if (i==str1.size() || j== str2.size())
                return L[i][j] = 0;

            else if (str1.at(i)==str2.at(j)){
                int temp =  L[i][j] =  1 + find_lcs(i+1, j+1);
                lcs_vector.push_back(make_pair(temp,str1.at(i)));
                return temp;
            }
            else
                return L[i][j] =  max (  find_lcs(i+1, j) ,  find_lcs(i, j+1));
        }
        else  return L[i][j];

      }

      void print_L_matrix(){
          cout<<setw(2)<<""<<setw(4)<<setw(4)<<"";
          for (int i = 0; i<str2.length(); i++)
              cout<<setw(4)<<str2.at(i);
          cout<<endl<<endl;

          for (int i = 0; i<=str1.length(); i++){
               i>0 ? cout<<setw(2)<<str1.at(i-1): cout<<setw(2)<<"";
               for (int j = 0; j<= str2.length(); j++)
                     cout<<setw(4)<<L[i][j];

               cout<<endl;
          }
          cout<<endl;
      }

      string build_lcs_str (){

          pair<int, char> temp = lcs_vector[lcs_vector.size()-1];
          int  current_val = L[0][0];

          lcs_str = "";
          for (int i=lcs_vector.size()-1; i>=0; i-- ){
               temp = lcs_vector[i];
               if (temp.first == current_val){
                   current_val --;
                  lcs_str += temp.second;
               }
          }
          lcs_str+='\0';
          return lcs_str;
      }
};

#endif // LCS_Recursion_CPP

