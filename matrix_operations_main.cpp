/***
Basic Matrix manipulations
*/
#include "matrix_operations.cpp"

using namespace std;

int main(){
    int size_m;
    cin>>size_m;

    matrix a(size_m, 0),  c(size_m, -1);

    a.input_m();
    a.print_m();

    a+=a;
    a.print_m();
    c= a*a;
    c.print_m();
    c= a;
    c.print_m();
    return 0;
}
