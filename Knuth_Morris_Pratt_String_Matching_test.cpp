/***
Knuth-Morris-Pratt (KMP) String Matching Algorithm
Main file
*/

#include "Knuth_Morris_Pratt_String_Matching.cpp"

int main(){
    string s1, s2;
    cout<<"********************\n";
    cout<<"Knuth-Morris-Pratt (KMP) String Matching Algorithm.\n";
    cout<<"Input: Two strings\n";
    cout<<"occurrence of the second string is searched inside the first\n";
    cout<<"********************\n";

    //cin>>s1>>s2;
    getline (cin,s1);
    getline (cin,s2);
    cout<<s1<<endl<<s2<<endl;
    Knuth_Morris_Pratt_Algorithm  kmp (s1, s2) ;
    kmp.search_str();
    cout<<endl;
    cout<<"Pattern found at indices:";
    kmp.print_results();
 }
