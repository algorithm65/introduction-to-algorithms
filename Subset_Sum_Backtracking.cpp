/***
Subset Sum Problem using Backtracking

https://link.springer.com/chapter/10.1007%2F978-3-540-24777-7_4
*/

#ifndef Subset_Sum_Backtracking_CPP
#define Subset_Sum_Backtracking_CPP

#include <iostream>
#include <iomanip>

using namespace std;

class Subset_Sum_Backtrack{

      long    bigset_size, subset_size;
      int    *big_set;
      int    *sub_set;
      long    target_sum;
      long    total_sol;
      long    call_count;

  public:
      Subset_Sum_Backtrack(int sum, int n){
          this->call_count = 0;
          this->total_sol  = 0;

          this->bigset_size = n;
          this->subset_size = 0;
          this->target_sum  = sum;

          this->big_set = new int [n];
          this->sub_set = new int [n];
      }

      ~Subset_Sum_Backtrack(){
          delete [] big_set;
          delete [] sub_set;
     }

     int get_total_sol(){
         return total_sol;
     }

     void input(){
         for(int i=0; i< bigset_size; i++)
             cin>>big_set[i];
     }

     void find_sum(int sum, int index){

          //cout<<"call_count: "<<call_count++<<endl;

          if (index>bigset_size)
              return;
          else if (sum>target_sum)
              return;
          else if (sum == target_sum ) {
             total_sol++;
             cout<<"solution no."<<total_sol<<endl;
             print_sub_array();
             return ;
          }
          else
             for(int i=index; i< bigset_size; i++){
                 sub_set[subset_size++] = big_set[i];
                 find_sum(sum + big_set[i], i+1 );
                 sub_set[subset_size--] = 0;
            }
      }

      void print_sub_array(){
          cout<<"{";
          for (int i = 0; i< subset_size ; i++){
               cout<<setw(3)<<sub_set[i];
               if(i<subset_size-1)cout<<",";
          }
          cout<<"}"<<endl;
      }

      void print_big_set(){
          cout<<"{";
          for (int i = 0; i< bigset_size ; i++){
               cout<<setw(3)<<big_set[i];
               if(i<subset_size-1)cout<<",";
          }
          cout<<"}"<<endl;
      }

  };

#endif // Subset_Sum_Backtracking_CPP

