/***
 Job Sequencing with Deadline - Branch and Bound

http://web-static.stern.nyu.edu/om/faculty/pinedo/scheduling/shakhlevich/handout06.pdf
*/

#ifndef Job_Sequencing_with_Deadline_BB
#define Job_Sequencing_with_Deadline_BB

#include <iostream>
#include <iomanip>
#include <queue>
#include <set>

using namespace std;

class node{
  public:
    set<int> jobs;
    long     index;

    node (){
      index = -1;
    }
};


class Job_Sequencing_BB{

      long    no_of_jobs;
      float  *penalty, *deadline, *time;
      float   upper, cost;
      long    call_count;
      queue<node> nodes_queue;
      set<int> min_set;

  public:
      Job_Sequencing_BB(int n){
          this->call_count = 0;
          this->no_of_jobs = n;

          this->penalty  = new float [n];
          this->deadline = new float [n];
          this->time     = new float [n];
      }

      ~Job_Sequencing_BB(){
          delete [] penalty;
          delete [] deadline;
          delete [] time;
     }

     void input(){
         for(int i=0; i< no_of_jobs; i++)
             cin>>penalty[i]>>deadline[i]>>time[i];
     }

     set<int> get_min_set(){
         return min_set;
     }

    void print_set(set<int> jobs){
         cout<<" { ";
         int c = 0;
         for (int i=0; i<no_of_jobs; ++i)
              if (jobs.count(i) != 0){
                  cout<<"j"<<i;
                  if (c++<jobs.size()-1) cout<< ", ";
              }
          cout<<" } ";
     }
     float find_time(set<int> jobs, int index){
         int t = 0;
         for (int i=0; i<=index; ++i)
             if (jobs.count(i) != 0)
                  t +=time[i];
          return t;
     }
     float find_cost(set<int> jobs, int index){
         int c = 0;
         for (int i=0; i<=index; ++i)
             if (jobs.count(i) == 0)
                  c +=penalty[i];
          return c;
     }
     float find_upper(set<int> jobs){
         int u = 0;
         for (int i=0; i<no_of_jobs; ++i) {
             if (jobs.count(i) == 0)
                  u +=penalty[i];
         }
         return u;
     }

     void start_search(){
          upper  = INT_MAX;
          cost   = 0;
          node temp_n;
          temp_n.index = -1;
          nodes_queue.push(temp_n);
          search_job_seq();
     }

      void search_job_seq(){
          //cout<<"call_count: "<<call_count++<<endl;
          float temp_u = 0;
          float temp_c = 0;
          float temp_t =  0;

          if (nodes_queue.empty())
              return;

           node front_node = nodes_queue.front();
           nodes_queue.pop();

           temp_u = find_upper(front_node.jobs);
           temp_c = find_cost(front_node.jobs, front_node.index);
           temp_t = find_time(front_node.jobs, front_node.index);

           if (front_node.index !=-1){
                 cout<<"set:";
                 print_set( front_node.jobs);
                 cout<<"upper:"<<temp_u<<", cost:"<<temp_c<<", time:"<<temp_t<<endl;
           }

           for (int i=front_node.index+1; i<no_of_jobs; i ++){

                front_node.jobs.insert(i);
                temp_u = find_upper(front_node.jobs);
                temp_c = find_cost(front_node.jobs, i);

                if (temp_c > upper || temp_t > deadline[i] || temp_t > cost )  {
                        front_node.jobs.erase(i);
                        continue;
                }

                if (temp_u < upper) {
                        upper = temp_u;
                        min_set = front_node.jobs;
                }

                if (temp_c > cost)  cost  = temp_c;

                node temp_n;
                temp_n.jobs=front_node.jobs;
                temp_n.index = i;

                nodes_queue.push(temp_n);
                front_node.jobs.erase(i);
          }
          search_job_seq();

      }

      void print_jobs(){

          for (int i = 0; i< no_of_jobs ; i++)
               cout<<setw(3)<<"j"<<i;
          cout<<endl;
          for (int i = 0; i< no_of_jobs ; i++)
               cout<<"----";
          cout<<endl;
          for (int i = 0; i< no_of_jobs ; i++)
               cout<<setw(4)<<penalty[i];
          cout<<endl;
          for (int i = 0; i< no_of_jobs ; i++)
               cout<<setw(4)<<deadline[i];
          cout<<endl;
          for (int i = 0; i< no_of_jobs ; i++)
               cout<<setw(4)<<time[i];
          cout<<endl;
      }
  };
#endif // Job_Sequencing_with_Deadline_BB

