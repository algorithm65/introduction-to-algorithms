/***
 Matrix array class
 */
#include <iostream>
#include "matrix_grid.cpp"

int main(){
    int size_m;
    cin>>size_m;
    matrix a(size_m, 0), d;
    a.input_m();
    a.print_m();

    matrix_grid m_g(a,2);
    m_g.print_grid();

    d = m_g.join_m ();
    d.print_m();
}
