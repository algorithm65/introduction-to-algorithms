

#include <iostream>
#include "merge_sort.cpp"

using namespace std;

int main(){
    int n, value;
    cout<<"Enter the total element number, followed by the elements\n";
    cin>>n;
    int *list_a = new int[n];
    merge_sort m_s(list_a,n);
    m_s.input_list();
    m_s.print_a();
    m_s.sort_a(list_a,0,n-1);
    cout<<endl<<"Sorted list"<<endl;
    for (int i=0; i<n;i++)
            cout<<list_a[i]<<" ";

    delete list_a;
}
